<?xml version='1.0' encoding='utf-8'?>
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
// local reference to another doc
| ]>

// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>


// categories: std | bcp | info | exp | historic
rfc(category='info' number='2966066' consensus='false' ipr='trust200902' docName='fras-jacks-tests-01' submissionType='be-cem-mro' xml:lang='en' version='3')
  front
    title(abbrev='Vertical and Radial Jacks tests') FRAS Radial and Vertical jacks tests
    author(fullname='Sylvain Fargier' initials='S.F.' role='editor' surname='Fargier')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email sylvain.fargier@cern.ch
    author(fullname='Samuel Wilson' initials='S.W.')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email samuel.wilson@cern.ch
    date(year='2023' month="November" day="15")

    keyword SCT
    keyword FRAS
    keyword SAMbuCa
    keyword tests

    abstract
      t This document describes tests achieved to measure Radial and Vertical jacks gear-boxes accuracy.

  middle

    section
      name Introduction
      t
        | This document describes tests achieved to measure Radial and
        | Vertical jacks prototype gear-boxes accuracy to prepare the Single
        | Component Test (SCT) for Full Remote Alignment System (FRAS) project.

    section(anchor="SEC_TEST_REPORTS")
      name Test reports

      t
        | Full execution reports and data are available for each test here: #[eref(target="https://cernbox.cern.ch/s/f2Ptx5qEzGfI4V5")]

    section
      name Test tools

      t
        | The #[xref(target="REF_SAMBUCA_FUNCTIONAL_TESTS") SAMbuCa function tests]
        | code-base has been used to implement and automate radial jacks testing.

      t
        | Those tests are implemented using #[xref(target="REF_RFW_TESTER") BE-CEM-MRO test utilities],
        | which are built on the top of #[xref(target="REF_ROBOTFRAMEWORK") RobotFramework].

    section
      name Test environment

      t
        | The tests described below were achieved on a test-bench that was designed
        | and prepared especially for SCT, it is equiped with SHS drivers and a
        | SAMbuCa Motion Front-End (MFE) system.

      t
        | Here's the list of components used:
      ul
        li SAMbuCa MFE card: ref:#[tt 00-20147-e1-0005]
        li Sylvac Palmer: 1ųm accuracy, ref:#[tt 911819]
        li Encoder: 1024 pulses/turn, ref:#[tt DRS60-ADL01024 1030339]

      t
        | Since SAMbuCa MFE system is still under-development, the resolver
        | acquisition and accuracy was tested on a separate motor/resolver before
        | each test, this measurement is described in #[xref(target="TEST_RESOLVER_ANGULAR_PRECISION")]
        | reports are available with the #[xref(target="SEC_TEST_REPORTS") execution reports].

    section(anchor="TEST_RESOLVER_ANGULAR_PRECISION")
      name Resolver Angular Precision Test

      t
        | The resolver angular precision test has been used to measure SAMbuCa
        | MFE system accuracy before each test.

      t
        | This test requires a motor and resolver with the following setup:
      ul
        li resolver must be mounted directly on the motor
        li stepper-motor must be able to achieve a complete revolution
        li stepper-motor must be configured in half-step (400 steps/turn)

      t
        | As an initial step the resolver angular position is acquired.

      t
        | The stepper motor is then actuated step by step and the resolver
        | angular value for each of those steps is measured.

      t
        | The relative angular displacement (initial resolver angular position -
        | measured position) is then compared with the stepper-motor theoretical
        | position (nb_steps / 400 steps/turn * pi).

      t
        | The complete test-sequence can be found here: #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/fmc_mfe/Resolvers.robot#L35") Resolvers.robot#L35]

      figure
        name Resolver Angular Precision measurement (sample data)
        artwork(align='center' type='binary-art' src='fras-jacks-tests/fmc_mfe_resolvers_angular_precision.png')

    section(anchor="TEST_SANITY_BASIC")
      name Motorized jack sanity tests

      t
        | Some sanity tests were achieved on the motorized jacks to ensure proper wiring and mechanical sanity:
      table(align='center')
        name Motrized jacks sanity tests
        thead
          tr
            th(align='center') Test
            th(align='center') Description
            th(align='center') Vertical Jack Reference
            th(align='center') Radial Jack Reference
        tbody
          tr
            td(align='center') hardware limits
            td(align='center') move the motor from one limit to the other
            td(align='center') #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/VerticalMotrizedJack.robot#L44") VerticalMotrizedJack.robot#L44]
            td(align='center') #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/RadialMotorizedJack.robot#L44") RadialMotrizedJack.robot#L44]
          tr
            td(align='center') resolver precision
            td(align='center') resolver accuracy test
            td(align='center') #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/VerticalMotrizedJack.robot#L49") VerticalMotrizedJack.robot#L49]
            td(align='center') #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/RadialMotorizedJack.robot#L49") RadialMotrizedJack.robot#L49]

      t
        | Those tests were not meant to record any data, so no reports have been kept,
        | but using those to ensure that basic operation can be achieved on the
        | hardware is quite important.

    section(anchor="TEST_ACCURACY")
      name Motorized jacks accuracy tests

      t
        | For all the accuracy tests the following settings have been used:
      table
        thead
          tr
            th(align='center') Jack
            th(align='center') resolver reduction
            th(align='center') output reduction
            th(align='center') output resolution [step/mm]
            th(align='center') reference
        tbody
          tr
            td(align='center') Vertical Jack
            td(align='center') 1946 (139*14)
            td(align='center') 7784 (139*14*4)
            td(align='center') 148266 (400*139/1.5*4)
            td(align='center') #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/VerticalMotrizedJack.robot#L22") VerticalMotrizedJack.robot#L22]
          tr
            td(align='center') Radial Jack
            td(align='center') 8000 (40/15*50*60)
            td(align='center') 240000 (40/15*50*60*30)
            td(align='center') 619200 (((400*40/15*50*30)/2*3.87)/5)
            td(align='center') #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/RadialMotorizedJack.robot#L22") RadialMotrizedJack.robot#L22]


      section(anchor="TEST_POSITION_MEASUREMENT")
        name Motorized jacks position measurement

        t
          | This test measures the mechanic's precision on the complete axis range,

        t
          | It requires the following setup:
        ul
          li a Radial or Vertical jack
          li a palmer mounted on the output piston for the Vertical jack
          li an encoder mounted on the output cam for the Radial jack
          li a SAMbuCa MFE system to control the stepper-motor and acquire the resolver positions

        t
          | As an initial step, the jacks are homed on its inner-switch,
          | controller and resolver position are then reset, encoder or palmer
          | reference position is measured.

        t
          | Then the jack is actuated with #[tt 0.005 mm] motions, measuring
          | the resolver and output position at each step.

        t
          | This is repeated until the outer-switch limit is reached.

        t
          | The test is implemented at the following locations:
        ul
          li #[strong Vertical Jack]: #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/VerticalMotrizedJack.robot#L67") VerticalMotrizedJack.robot#L67]
          li #[strong Radial Jack]: #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/RadialMotorizedJack.robot#L68") RadialMotrizedJack.robot#L68]

        figure
          name Vertical jack position measurement (sample data)
          artwork(align='center' type='binary-art' src='fras-jacks-tests/vertical_jack_position.png')

      section(anchor="TEST_BACKLASH_MEASUREMENT")
        name Motorized jacks backlash measurement

        t
          | This test measures the mechanic's backlash by moving back and forth to
          | a given position.

        t
          | In this test the axis is moved over 10% of the total axis range back and
          | forth, it is controlled turn by turn (400 steps), measuring the resolver position
          | after each motion.

        t
          | Since this test is quite repetitive, it has also been used to measure
          | the mechanic's repeatability by comparing the angular error at a fixed-point.

        t
          | It is also one of the most intensive tests we ran, actuating the device for
          | hours, acting like an endurance test.

        t
          | It requires the following setup:
        ul
          li a Radial or Vertical jack
          li a SAMbuCa MFE system to control the stepper-motor and acquire the resolver positions

        t
          | The test is implemented at the following locations:
        ul
          li #[strong Vertical Jack]: #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/VerticalMotrizedJack.robot#L58") VerticalMotrizedJack.robot#L58]
          li #[strong Radial Jack]: #[eref(target="https://gitlab.cern.ch/mro/controls/sambuca/tests/-/blob/bb197ebccbaafb938431db2e6134809107a79642/tests/SingleComponentTest/RadialMotorizedJack.robot#L58") RadialMotrizedJack.robot#L58]

        figure
          name Vertical jack backlash (sample data)
          artwork(align='center' type='binary-art' src='fras-jacks-tests/vertical_jack_backlash_zoom.png')

        t
          | On the Figure above you can observe the backlash on the measured error
          | for both turn-directions, either there's a direct gap in the measured
          | error (see #[tt backlash zoom 1]), or a vertical shift of the resolver
          | error (see #[tt backlash zoom 2]).

      section
        name Test Results

        t
          | Here are the results from the tests described above.

        t
          | Those results needs to be interpreted keeping in mind our bench accuracy is about #[tt 1ųm].

        table
          name Motorized jacks position measurement test results
          thead
            tr
              th(align='center') Jack
              th(align='center') Jack Type
              th(align='center') resolver/output error amplitude (max-min) [ųm]
              th(align='center') resolver/output error stdev
              th(align='center') resolver/output error mean [ųm]
          tbody
            tr
              td(align='center') 23060001
              td(align='center') vertical
              td(align='center') 13.39
              td(align='center') 2.74
              td(align='center') 5.37
            tr
              td(align='center') 23060002
              td(align='center') vertical
              td(align='center') 9.29
              td(align='center') -1.143
              td(align='center') 1.748
            tr
              td(align='center') 23060003
              td(align='center') vertical
              td(align='center') 22.12
              td(align='center') -0.957
              td(align='center') 4.716
            tr
              td(align='center') 23040001
              td(align='center') radial
              td(align='center') 14.292
              td(align='center') 2.335
              td(align='center') 3.406
            tr
              td(align='center') 23040002
              td(align='center') radial
              td(align='center') 6.746
              td(align='center') 1.456
              td(align='center') 2.757

        table
          name Motorized jacks backlash test results
          thead
            tr
              th(align='center') Jack
              th(align='center') Jack Type
              th(align='center') resolver/controller error amplitude (max-min) [ųm]
              th(align='center') estimated backlash [ųm]
              th(align='center') repeatibility error amplitude (max-min) [ųm]
          tbody
            tr
              td(align='center') 23060001
              td(align='center') vertical
              td(align='center') 9
              td(align='center') ~0.3
              td(align='center') 0.12
            tr
              td(align='center') 23060002
              td(align='center') vertical
              td(align='center') 3.99
              td(align='center') ~0.25
              td(align='center') 0.067
            tr
              td(align='center') 23060003
              td(align='center') vertical
              td(align='center') 6.63
              td(align='center') ~0.25
              td(align='center') 0.28
            tr
              td(align='center') 23040001
              td(align='center') radial
              td(align='center') 1.305
              td(align='center') ~0.01
              td(align='center') 0.107
            tr
              td(align='center') 23040002
              td(align='center') radial
              td(align='center') 1.047
              td(align='center') ~0.03
              td(align='center') 0.051

    section
      name Conclusion

      t
        | Radial and vertical gear-boxes for FRAS are quite precise,
        | most of the observed errors are related to the resolvers accuracy or
        | testing material.

      t
        | All jacks did break at least once during the tests and had to be repaired or
        | consolidated, those tests helped in revealing such weaknesses but
        | do not guarantee in any way that those motors will be robust and long-lived
        | once installed.

      t
        | The mechanical end-limit (security mechanism) was tested separately,
        | so far we did not achieve tests with maximum torque and the intial
        | bronze version of this limit broke on first test, vibrations are quite
        | high on this pin especially when the stepper-motor slows down.
        | The pin was replaced by a stainless-steel pin which seems to support
        | the load.

      t
        | Two axis were damaged by going further away than the hardware-limit,
        | resolvers and other pieces (spring) had to be replaced, this may
        | happen during operation, the hardware-limit being a mobile part operated
        | by a spring.

      t
        | Gears went loose on their axis several times (especially on Radial Jacks),
        | this is where most of the issues came from, we do recommend in adding
        | and using keys on the cams.

      t
        | The Radial Jacks are also quite noisy, way more than the Vertical Jacks,
        | this doesn't seem to impact accuracy but feels a bit weird to hear
        | those terrible noises (we had some complaints from people working nearby).

      t
        | It is a bit unusual to have the resolver after the gear-box,
        | still it seems to work properly and we can eventually activate
        | step-lost detection (using a very large threshold that would be
        | significant with regards to the reduction ratio).

      t
        | It is to be noted that the resolver, hardware-limit and limit-switches
        | are not located on the same reduction lane as the output,
        | thus if this one breaks the stepper may freely move the jack off-limits
        | making the user think that nothing moves.

  back
    references
      name References
      references
        name Informative References

      references
        name URL References
        reference(anchor='REF_SAMBUCA_FUNCTIONAL_TESTS' target='https://gitlab.cern.ch/mro/controls/sambuca/tests')
          front
            title SAMbuCa functional tests code-base
            author
              organization CERN
            date(year='2023')

        reference(anchor='REF_RFW_TESTER' target='https://gitlab.cern.ch/mro/common/tools/tests/rfw-tester')
          front
            title BE-CEM-MRO RobotFramework test utilities
            author
              organization CERN
            date(year='2023')

        reference(anchor='REF_ROBOTFRAMEWORK' target='https://robotframework.org/')
          front
            title Robot Framework is a generic open source automation framework
            author
              organization Robot Framework Foundation
            date(year='2008')
          annotation #[tt License: Apache-2.0]
