<?xml version='1.0' encoding='utf-8'?>
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
<!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY rfc5234 SYSTEM "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5234.xml">
// local reference to another doc
| ]>

// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>


// categories: std | bcp | info | exp | historic
rfc(category='std' number='2417680' consensus='false' ipr='trust200902' docName='en-smm-apc-web-guidelines-01' submissionType='be-cem-mro' obsoletes='' updates='' xml:lang='en' version='3' xmlns:xi='http://www.w3.org/2001/XInclude')
  front
    title(abbrev='APC Web Development Guidelines') APC Web Development Guidelines
    author(fullname='Sylvain Fargier' initials='S.F.' role='editor' surname='Fargier')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email sylvain.fargier@cern.ch
    date(year='2019' month="December" day="11")

    keyword WEB

    abstract
      t
        | This document describes EN-SMM-APC team WebSite and Web Applications
        | development guidelines.


  middle

    section
      name Introduction
      t
        | This document is a reference document for the EN-SMM-APC team,
        | describing the WebSite and Web Application development recommendations
        | and requirements.

      t
        | For specific tips &amp; tricks, please refer to
        | #[xref(target="NEWCOMER_WIKI") Newcomer Corner Wiki].

      // This one is a 'must' :)
      section
        name Requirements Notation
        t
          | The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
          | "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
          | document are to be interpreted as described in BCP 14,
          | #[xref(target='RFC2119' format='default') RFC2119].

    section(anchor="Backend")
      name Back-end development (Node.js)

      t
        | Back-end development #[bcp14 MAY] be achieved using the #[tt Node.js]
        | framework, it is not limited to this language and those #[bcp14 MAY]
        | even be mixed (using Docker containers).

      t
        | Back-end or server-side development includes any Javascript project
        | that uses the #[tt Node.js] framework, including web-servers as well
        | as command-line utils and application.

      section(anchor="Backend_Layout")
        name Layout

        t
          | The following directory layout #[bcp14 SHOULD] be used for back-end
          | projects:
        table
          name Back-end directory layout
          thead
            tr #[th Directory/File] #[th Description]
          tbody
            tr #[td /package.json] #[td Package description file]
            tr #[td /test/test_*.js] #[td Unit-test files]
            tr #[td /src/**.js] #[td Source files]
            tr #[td /www/] #[td Front-end project directory (optional)]
            tr
              td .jshintrc .eslintrc.yml .eslintignore
              td Linter configuration files, see #[xref(target="Backend_Linting")].
            tr
              td /flow-type/
              td Type-checking files, see #[xref(target="Backend_TC")].
            tr
              td .gitlab-ci.yml
              td Continuous Integration configuration file, see #[xref(target="Backend_CI")]


      section(anchor="Backend_Cmd")
        name Command Line Utilities

        t
          | Back-end projects #[bcp14 MUST] implement the following #[tt npm]
          | scripts:
        table
          name Back-end npm scripts
          thead
            tr #[th Command] #[th Description]
          tbody
            tr #[td help] #[td display npm scripts help]
            tr #[td info] #[td alias for help script]
            tr #[td install] #[td install dependencies (npm install ...)]

            tr #[td test] #[td run unit-tests]
            tr #[td test:ci] #[td run unit-tests in CI mode]
            tr #[td test:watch] #[td run unit-tests in server mode, watching for file changes]

            tr #[td lint] #[td run linting tools]
            tr #[td lint:watch] #[td run linting tools in server mode, watching for file changes]
            tr #[td lint:ci:builder] #[td run linting tools in CI mode]
            tr #[td lint:ci:report] #[td generate CI linter badge and report]

            tr #[td tc] #[td run TypeChecking tools]
            tr #[td tc:ci:builder] #[td run TypeChecking tools in CI mode]
            tr #[td tc:ci:report] #[td generate CI TypeChecking badge and report]

            tr #[td watch] #[td generic watch command for the back-end]

        t
          | Back-end projects having a #[xref(target="Frontend") Front-end]
          | sub-project #[bcp14 SHOULD] implement the following #[tt npm]
          | scripts:
        table
          name Back-end npm scripts for front-end sub-project
          thead
            tr #[th Command] #[th Description]
          tbody
            tr #[td serve] #[td run Back-end server part]
            tr #[td serve:dev] #[td run Build Front-end and run Back-end watching for changes on both]
            tr #[td serve:watch] #[td run Back-end server part, watching for file changes]

            tr #[td test:front] #[td run front-end unit-tests]
            tr #[td test:front:watch] #[td run front-end unit-tests]

            tr #[td build] #[td build for development/debugging]
            tr #[td build:prod] #[td build for production]

            tr #[td lint:front] #[td run linting tools on the front-end]
            tr #[td lint:front:watch] #[td run linting tools on the front-end in server mode, watching for file changes]

            tr #[td tc:front] #[td run TypeChecking tools on the front-end]

            tr #[td watch:front] #[td generic watch command for the front-end]

        t
          | #[strong Note:] #[tt *:ci] scripts #[bcp14 MUST] run tests or
          | linting tools on both the front-end and back-end.

        t
          | #[strong Note:] additional npm scripts #[bcp14 MAY] be implemented,
          | still too much additions makes projects fuzzy and is not
          | #[bcp14 RECOMMENDED].

        t
          | It is #[bcp14 RECOMMENDED] to use #[tt npm-run-all] and
          | #[tt npm-scripts-info] modules to implement and document scripts.

      section(anchor="Backend_Deps")
        name Recommended Libraries

        t
          | Here is a set of #[bcp14 RECOMMENDED] libraries for
          | back-end implementation:
        table
          name Recommended libraries for back-ends
          thead
            tr #[th library] #[th Description]
          tbody
            tr #[td #[xref(target='EXPRESS') express]] #[td WebServer]
            tr #[td #[xref(target='EXPRESS_WS') express-ws]]
              td
                | WebSocket plugin for #[tt express] relying
                | on #[xref(target='WS') ws] library.
            tr #[td #[xref(target='PASSPORT') passport]] #[td Authentication]
            tr #[td #[xref(target='DEBUG') debug]] #[td logging and debugging library]
            tr #[td #[xref(target='PUG') pug]] #[td Pug HTML/XML templating engine]
            tr #[td #[xref(target='Q') q]] #[td Promise library]
            tr #[td #[xref(target='LODASH') lodash]] #[td utility library]

      section(anchor="Backend_Linting")
        name Linting

        t
          | Backend linting #[bcp14 SHOULD] be achieved using
          | #[xref(target='ESLINT') ESLint] and #[xref(target="JSHINT") JSHint].

        t
          | The #[tt lint] #[tt npm] scripts #[bcp14 MUST] run those tools on the
          | codebase.

        t
          | The main project branch (usualy #[tt master]) #[bcp14 MUST] not
          | have any linting issues.

      section(anchor="Backend_TC")
        name TypeChecking

        t
          | TypeChecking #[bcp14 MUST] be based on #[xref(target="TypeScript") TypeScript]
          | language, still the typescript file format #[bcp14 SHOULD NOT] be used,
          | #[xref(target="JSDoc") JSDoc comments] being the #[bcp14 RECOMMENDED] syntax,
          | see #[xref(target="TypeScript_JSDoc") TypeScript JSDoc] for details.

        t
          | TypeChecking coverage #[bcp14 SHOULD] be validated by #[xref(target="Backend_CI") CI],
          | projects #[bcp14 MUST] reach #[em 80%] type coverage,
          | TypeChecking #[bcp14 MUST] be validated
          | using #[xref(target="TypeScript_compiler") tsc].

        t
          | Each library #[bcp14 MUST] provide a
          | #[xref(target="TypeScript_declaration") TypeScript declaration file],
          | applications #[bcp14 MAY NOT] provide such declaration file.

        t
          | TypeScript declaration files #[bcp14 MUST] be validated using
          | #[xref(target="TypeScript_tsd") tsd].

        t
          | Additional information on the tools and recommendations
          | are available on the
          | #[xref(target="NEWCOMER_WIKI") Newcomer Corner Wiki].

        t | #[strong Note:] #[xref(target="FLOW") Flow] TypeChecking
          | #[bcp14 SHOULD NOT] be used anymore and has been deprecated,
          | mostly due to its instabilities and since latest typescript
          | compiler became an alternative by supporting JSDoc syntax.

      section(anchor="Backend_CI")
        name Continuous Integration

      section(anchor="Backend_TU")
        name Unit-Tests

        t
          | Back-end projects #[bcp14 MUST] be unit-tested, their code coverage
          | level #[bcp14 MUST] be above 70% and #[bcp14 SHOULD] be above 90%.

        t
          | Tests #[bcp14 MUST] be located in a #[tt test] directory at the
          | root of the back-end project.

        t
          | Unit-tests are written per-class or source-file, which is identified
          | as the subject-under-test, each test file #[bcp14 MUST] be named using
          | the following  #[xref(target="RFC5234") abnf syntax]:
        figure
          name Test files names
          sourcecode(type='abnf')
            | test_file = "test_" class_name "_" feature ".js"
            |
            | class_name = upper (ALPHA / DIGIT / "-" )*
            | upper = %x41-5A
            |
            | feature = (ALPHA / DIGIT / "-" / "+" / "_" / "." / ":")+


        t
          | Unit-tests #[bcp14 SHOULD] be implemented using
          | #[xref(target="MOCHA") Mocha] unit-test framework.

        t
          | The #[tt expect] API of #[xref(target="CHAI") Chai] assertion
          | library #[bcp14 SHOULD] be used, it is #[bcp14 RECOMMENDED] to use
          | #[xref(target="DIRTY_CHAI") Dirty-Chai] to use a safe and robust
          | notation.

        t
          | Any external dependency #[bcp14 MUST] be mocked or stubbed
          | whilst running Back-end tests, it #[bcp14 MAY] be achieved using
          | #[xref(target="SINON") Sinon] mocking library.

        t
          | Tests #[bcp14 MUST] cover more than 80% of the project lines on
          | the main project branch (usualy #[tt master]), it is #[bcp14 RECOMMENDED]
          | to have more than 90% of line coverage.
          | #[xref(target="NYC") Istanbul/NYC] #[bcp14 SHOULD] be used to
          | measure the code coverage.

    section(anchor="Frontend")
      name Front-end development

      t
        | Front-end or web development includes any browser based project.

      t
        | Such projects #[bcp14 MAY] be embedded in a #[tt www] directory within
        | a back-end project.

      section(anchor="Frontend_Layout")
        name Layout

        t
          | The following directory layout #[bcp14 SHOULD] be used for front-end
          | projects:
        table
          name Front-end directory layout
          thead
            tr #[th Directory/File] #[th Description]
          tbody
            tr #[td /package.json] #[td Package description file]
            tr #[td /test/test_*.js] #[td Unit-test files]
            tr #[td /src/**.js] #[td Source files]
            tr #[td /src/**.vue] #[td Vue component template and CSS files]
            tr #[td /src/**.vue.js] #[td Vue component Source files]
            tr #[td /www/] #[td Front-end project directory (optional)]
            tr
              td .jshintrc .eslintrc.yml .eslintignore
              td Linter configuration files, see #[xref(target="Frontend_Linting")].
            tr
              td /flow-type/
              td Type-checking files, see #[xref(target="Frontend_TC")].
            tr
              td .gitlab-ci.yml
              td Continuous Integration configuration file

        t #[strong Note:] this also applies to #[em www] directory of back-end projects.

      section(anchor="Frontend_Cmd")
        name Command Line Utilities

        t
          | Front-end projects #[bcp14 MUST] provide the same #[tt npm] scripts
          | than #[xref(target="Backend_Cmd") Back-end] along with #[tt build]
          | and #[tt build:prod] commands.

      section(anchor="Frontend_Deps")
        name Recommended Libraries

        t
          | Here is a set of #[bcp14 RECOMMENDED] libraries for
          | front-end implementation:
        table
          name Recommended libraries for front-ends
          thead
            tr #[th library] #[th Description]
          tbody
            tr #[td #[xref(target='VUE_JS') vue.js]] #[td Front-end engine]
            tr #[td #[xref(target='BOOTSTRAP') bootstrap]]
              td Widgets and CSS
            tr #[td #[xref(target='FONTAWESOME') fontawesome]] #[td Icons]
            tr #[td #[xref(target='GREENSOCK') greensock]] #[td Animations]
            tr #[td #[xref(target='DEBUG') debug]] #[td logging and debugging library]
            tr #[td #[xref(target='Q') q]] #[td Promise library]
            tr #[td #[xref(target='LODASH') lodash]] #[td utility library]

      section(anchor="Frontend_Linting")
        name Linting

        t
          | Frontend linting #[bcp14 SHOULD] be achieved using
          | #[xref(target='ESLINT') ESLint] and #[xref(target="JSHINT") JSHint].

        t
          | The #[tt lint] #[tt npm] scripts #[bcp14 MUST] run those tools on the
          | codebase.

        t
          | The main project branch (usualy #[tt master]) #[bcp14 MUST NOT]
          | have any linting issues.

      section(anchor="Frontend_TC")
        name TypeChecking

        t
          | Frontend TypeChecking #[bcp14 MUST] be achieved with the same tools
          | than the #[xref(target="Backend_TC") Backend].

      section(anchor="Frontend_TU")
        name Unit-Tests

        t
          | It is #[bcp14 RECOMMENDED] to use #[xref(target="KARMA") Karma] to run
          | the tests, the test framework and assertion libraries #[bcp14 SHOULD]
          | be the same than for back-end (see #[xref(target="Backend_TU")]).

        t
          | Any external dependency #[bcp14 MUST] be mocked or stubbed
          | whilst running Front-end tests.

        t
          | Tests #[bcp14 MUST] cover more than 80% of the project lines on
          | the main project branch (usualy #[tt master]), it is #[bcp14 RECOMMENDED]
          | to have more than 90% of line coverage.
          | #[xref(target="NYC") Istanbul/NYC] #[bcp14 SHOULD] be used to
          | measure the code coverage.

        t
          | Karma #[bcp14 MUST] be usable from the command-line, the following
          | options #[bcp14 MUST] be supported in addition to the karma default
          | built-in:
        ul
          li #[tt debug] Enable tests debugging mode
          li #[tt grep] Run specific tests, according to a provided regular expression

      section(anchor="Frontend_Vue")
        name Vue.js development

        t
          | To ease #[xref(target="Frontend_TC") TypeChecking] and
          | #[xref(target="Frontend_Linting") linting],
          | #[xref(target='VUE_JS') Vue] component files
          | #[bcp14 SHOULD] be split in template/CSS and source files:

        figure
          name Example Vue.js component template/CSS file
          sourcecode(type='Vue.js' src="en-smm-apc-web-guidelines/vue_template.vue")

        figure
          name Example Vue.js component source file
          sourcecode(type='javascript' src="en-smm-apc-web-guidelines/vue_component.vue.js")

        t
          | Vue components that are not application specific #[bcp14 SHOULD] be prefixed
          | with #[em Base], it is #[bcp14 RECOMMENDED] to prefix application
          | specific files with #[em App].
    section(anchor="Deploy")
      name Applications deployment

      section
        name Deployment Platform

        t
          | The #[xref(target="CERN_OPENSHIFT") CERN OpenShift]
          | container platform #[bcp14 SHOULD] be used to deploy WebSites and
          | Web Applications, this service will handle redundancy, load-balancing
          | and sanity checks as well as a clean deployment process that can be
          | automated.

        t
          | It is #[bcp14 RECOMMENDED] to build complete #[xref(target="DOCKER") Docker]
          | containers hosting parts or complete applications, those containers
          | #[bcp14 MAY] be built and hosted on #[xref(target="CERN_GITLAB") CERN Gitlab] registry.

      section
        name Deployment Process

        t
          | The #[tt master] branch is used as a delivery branch, and thus
          | #[bcp14 SHOULD] use a merge-request integration pattern (see
          | #[xref(target="GITLAB_CONFIG") GitLab Configuration project]
          | to automate projects configuration).

        t
          | The deployment process #[bcp14 SHOULD] be fully automated, using
          | the following scheme:
        figure
          name Web-App deployment process
          artwork(align='center' type='binary-art' src='en-smm-apc-web-guidelines/web-app-deploy.png')

        t
          | Docker images built on the top of an #[tt LTS] Node.js version
          | (ex: node:lts-alpine) #[bcp14 SHOULD] be used to build both #[tt latest]
          | and #[tt release] images.

        t
          | Images #[bcp14 MUST] not be built if
          | #[xref(target='Backend_TU') unit-tests] or
          | #[xref(target='Frontend_TU') functional-tests] are failing.

    section
      name Authentication

      t
        | It is #[bcp14 RECOMMENDED] to use #[xref(target='CERN_OAUTH') CERN OAuth2.0]
        | authentication scheme, filtering on #[tt e-groups] directly on the
        | back-end.

    section
      name Contributors
      // Optional section to mention contributors

  back

    references
      name References
      references
        name Normative References
        | &rfc2119;
        | &rfc5234;

      references
        name Informative References

      references
        name URL References
        reference(anchor='MOCHA' target='https://mochajs.org/')
          front
            title Mocha is a feature-rich JavaScript test framework running on Node.js and in the browser.
            author
              organization JS Foundation
            date(year='2011')

        reference(anchor='CHAI' target='https://www.chaijs.com/')
          front
            title Chai is a BDD/TDD assertion library for node and the browser.
            author
              organization Chai.js Assertion Library

        reference(anchor='DIRTY_CHAI' target='https://github.com/prodatakey/dirty-chai')
          front
            title Function form for terminating assertion properties.
            author(fullname='Joshua Perry' role='editor')

        reference(anchor='KARMA' target='http://karma-runner.github.io')
          front
            title A simple tool that allows you to execute JavaScript code in multiple real browsers.
            author
              organization Google

        reference(anchor="SINON" target='https://sinonjs.org/')
          front
            title Standalone test spies, stubs and mocks for JavaScript.
            author(fullname='Christian Johansen' role='editor')

        reference(anchor='NYC' target='https://istanbul.js.org/')
          front
            title JavaScript test coverage made simple.
            author(fullname='Isaac Schlueter' role='contributor')
            author(fullname='Mark Wubben' role='contributor')
            author(fullname='James Talmage' role='contributor')
            author(fullname='Krishnan Anantheswaran' role='contributor')

        reference(anchor="ESLINT" target='https://eslint.org')
          front
            title The pluggable linting utility for JavaScript and JSX
            author(fullname='Nicholas C. Zakas' role='editor')
              organization OpenJS Foundation
            date(year='2013')

        reference(anchor='JSHINT' target='https://jshint.com/about/')
          front
            title A Static Code Analysis Tool for JavaScript
            author(fullname='Anton Kovalyov' role='editor')
            date(year='2011')

        reference(anchor='DOCKER' target='https://docker.com')
          front
            title Docker provides a way to run applications securely isolated in a container, packaged with all its dependencies and libraries.
            author(fullname='Solomon Hykes' role='editor')
              organization Docker, Inc
            date(year='2013')


      references
        name CERN Services and Repositories

        reference(anchor='CERN_OPENSHIFT' target='https://openshift.cern.ch')
          front
            title PaaS Web Application Hosting Service
            author(fullname='Bruno Silva De Sousa - IT/CDA' role='editor')
              organization CERN
            date(year='2016')

        reference(anchor='CERN_GITLAB' target='https://gitlab.cern.ch')
          front
            title Git repository hosting platforms
            author(fullname='Alexandre Lossent - IT/CDA' role='editor')
              organization CERN
            date(year='2015')

        reference(anchor='CERN_OAUTH' target='https://oauth.web.cern.ch')
          front
            title CERN OAuth2.0 Service
            author(fullname='Emmanuel Ormancey - IT/CDA' role='editor')
              organization CERN
            date(year='2014')

        reference(anchor='GITLAB_CONFIG' target='https://gitlab.cern.ch/apc/common/tools/gitlab-config')
          front
            title GitLab configuration scripts for EN-SMM-APC section
            author(fullname='Sylvain Fargier - EN/SMM' role='editor')
              organization CERN
            date(year='2019')

        reference(anchor='NEWCOMER_WIKI' target='https://gitlab.cern.ch/apc/common/newcomer-corner/-/blob/master/WEBDEV.md')
          front
            title EN-SMM-APC Newcomer Corner Wiki
            author
              organization CERN
            date(year='2019')

      references
        name Recommended Libraries
        reference(anchor='EXPRESS' target='http://expressjs.com/')
          front
            title Fast, unopinionated, minimalist web framework for Node.js.
            author
              organization Node.js Foundation
            date(year='2010')
          annotation #[tt License: MIT]

        reference(anchor='PASSPORT' target='http://www.passportjs.org/')
          front
            title Simple, unobtrusive authentication for Node.js.
            author(fullname='Jared Hanson' role='editor')
            date(year='2011')
          annotation #[tt License: MIT]

        reference(anchor='WS' target='https://github.com/websockets/ws')
          front
            title
              | ws is a simple to use, blazing fast, and thoroughly tested
              | WebSocket client and server implementation.
            author(fullname='Einar Otto Stangvik' role='editor')
            date(year='2011')
          annotation #[tt License: MIT]

        reference(anchor='EXPRESS_WS' target='https://github.com/HenningM/express-ws')
          front
            title WebSocket endpoints for Express applications.
            author(fullname='Henning Morud' role='editor')
            date(year='2014')
          annotation #[tt License: AS-IS]

        reference(anchor='DEBUG' target='https://github.com/visionmedia/debug')
          front
            title A tiny JavaScript debugging utility modelled after Node.js core's debugging technique.
            author(fullname='TJ Holowaychuk' role='editor')
            author(fullname='Nathan Rajlich' role='editor')
            author(fullname='Andrew Rhyne' role='editor')
            date(year='2011')
          annotation #[tt License: MIT]

        reference(anchor='PUG' target='https://pugjs.org')
          front
            title Pug is a clean, whitespace sensitive syntax for writing html.
            author(fullname='TJ Holowaychuk' role='editor')
            date(year='1996')
          annotation #[tt License: MIT]

        reference(anchor='Q' target='https://github.com/kriskowal/q')
          front
            title A library for promises (CommonJS/Promises/A,B,D)
            author(fullname='Kris Kowal' role='editor')
            date(year='2010')
          annotation #[tt License: AS-IS]

        reference(anchor='VUE_JS' target='https://vuejs.org/')
          front
            title The Progressive JavaScript Framework
            author(fullname='Evan You' role='editor')
            date(year='2014')
          annotation #[tt License: MIT]

        reference(anchor='BOOTSTRAP' target='https://getbootstrap.com/')
          front
            title An open source toolkit for developing with HTML, CSS, and JS
            author(fullname='Mark Otto' role='editor')
            author(fullname='Jacob Thornton' role='editor')
            date(year='2011')
          annotation #[tt License: MIT]

        reference(anchor='FONTAWESOME' target='https://fontawesome.com')
          front
            title The web's most popular icon set and toolkit
            author(fullname='Dave Gandy' role='editor')
              organization FontAwesome
            date(year='2012')
          annotation #[tt License: Freemium]

        reference(anchor='LODASH' target='https://lodash.com/')
          front
            title A modern JavaScript utility library delivering modularity, performance &amp; extras
            author(fullname='John-David Dalton' role='editor')
            date(year='2012')
          annotation #[tt License: MIT]

        reference(anchor='GREENSOCK' target='https://greensock.com/')
          front
            title Ultra high-performance, professional-grade animation for the modern web
            author
              organization GreenSock
            date(year='2008')
          annotation #[tt License: GreenSock]

        reference(anchor='FLOW' target='https://flow.org/')
          front
            title A static type checker for JavaScript
            author
              organization Facebook Open Source
            date(year='2014')
          annotation #[tt License: MIT]

        reference(anchor="TypeScript" target="https://www.typescriptlang.org/")
          front
            title Typed JavaScript at Any Scale
            author
              organization Microsoft
            date(year='2012')
          annotation #[tt License: Apache 2.0]

        reference(anchor="JSDoc" target="https://jsdoc.app/")
          front
            title JSDoc is a markup language used to annotate JavaScript source code files.
            author
              organization Netscape/Mozilla
            date(year='1999')
          annotation #[tt License: Open format]

        reference(anchor="TypeScript_JSDoc" target="https://www.typescriptlang.org/docs/handbook/intro-to-js-ts.html")
          front
            title How to add type checking to JavaScript files using TypeScript
            author
              organization Microsoft
            date(year='2012')
          annotation #[tt License: Apache 2.0]

        reference(anchor="TypeScript_compiler" target="https://github.com/Microsoft/TypeScript")
          front
            title the TypeScript compiler
            author
              organization Microsoft
            date(year='2012')
          annotation #[tt License: Apache 2.0]

        reference(anchor="TypeScript_declaration" target="https://www.typescriptlang.org/docs/handbook/declaration-files/introduction.html")
          front
            title TypeScript declaration files
            author
              organization Microsoft
            date(year='2012')
          annotation #[tt License: Apache 2.0]

        reference(anchor="TypeScript_tsd" target="https://github.com/SamVerschueren/tsd#readme")
          front
            title TypeScript definition files checker
            author(fullname='Sam Verschueren' role='editor')
            date(year='2014')
          annotation #[tt License: MIT]

    section
      name Change Log
      t First version

    //- section
    //-   name Open Issues
    //-   t A list of open issues regarding this document
