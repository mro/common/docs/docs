<?xml version='1.0' encoding='utf-8'?>
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
<!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
// local reference to another doc
| ]>

// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>


// categories: std | bcp | info | exp | historic
rfc(category='std' consensus='false' ipr='trust200902' docName='plc-guidelines-01' submissionType='be-cem-mro' xml:lang='en' version='3')
  front
    title(abbrev='PLC software guidelines') BE-CEM-MRO PLC software coding guidelines
    author(fullname='Mathieu Donze' initials='M.D.' role='editor' surname='Donze')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email mathieu.donze@cern.ch
    date(year='2022' month="May" day="18")

    keyword PLC

    abstract
      t
        | This document describes PLC development guidelines and
        | standards to be used in the BE-CEM-MRO section.

  middle

    section
      name Introduction
      t
        | This document describes what technologies #[bcp14 MUST] be used and how
        | PLC software #[bcp14 MUST] be architectured in BE-CEM-MRO section.
        | Guidelines in this document #[bcp14 MUST] be followed by every person
        | developping PLC projects in the section.

      // This one is a 'must' :)
      section
        name Requirements Notation
        t
          | The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
          | "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
          | document are to be interpreted as described in BCP 14,
          | #[xref(target='RFC2119' format='default') RFC2119].

    section(anchor="CHOOSING_HW")
      name Choosing hardware
      t
        | To keep a small number of spare parts and to avoid mixing technologies, PLC-based projects
        | #[bcp14 MUST] use Siemens automation hardware everywhere it's possible.

      t
        | New PLC projects #[bcp14 MUST] use S7-1500 product family, S7-1200 products #[bcp14 MAY]
        | also be used if the PLC project is simple and compact.

      t
        | The PLC CPU #[bcp14 SHOULD]
        | have a built-in display for on-site diagnostics and configuration.

      t
        | Fieldbus communication #[bcp14 MUST] be based on Profinet. Older projects #[bcp14 MAY]
        | use Profibus communication. Other fieldbuses #[bcp14 MAY] be used if some components
        | doesn't support standards mentionned above.

    section(anchor="DEV_SW")
      name Development software
      t
        | New PLC software projects #[bcp14 MUST] be developed using Siemens TIA-Portal software suite.
        | To avoid migrating projects version, it was choosed to stay at TIA-Portal version 16.

      t
        | If a new hardware component doesn't support this software version the project #[bcp14 SHOULD]
        | use a newer version of the TIA portal application.

      t
        | Older existings projects using Step 7 software suite must be kept with this version until supported
        | on actual operating system.

      t
        | WinCC #[bcp14 MUST] be used to develop panels graphical user interfaces but #[bcp14 MUST NOT] be used
        | for creating expert or piquet interfaces.

      t
        | VersionDog and #[xref(target="git") Git] is also used for code versioning. See the #[xref(target="CODE_VERSIONING")] chapter.

    section(anchor="CODE_VERSIONING")
      name Code versioning workflow
      t
        | In order to keep track of PLC project modifications during development phase but
        | also during production stage, two code versioning tool #[bcp14 MUST] be used.

      t
        | #[xref(target="git") Git] #[bcp14 MUST] be used for storing PLC code changes during development phase.
        | This tool is well-known from all software developers and it's usage is out of this document's scope.

      t
        | The code repository location #[bcp14 MUST] be located in #[xref(target="git_repo") MRO PLC Git repository], so other developers can review
        | the PLC project.

      t
        | When the PLC project goes to production phase, the operational PLC project must be stored using
        | #[xref(target="cern_versiondog") VersionDog] software.

      t
        | The #[xref(target="git_repo") MRO PLC Git repository] master branch #[bcp14 SHOULD] match the one stored in VersionDog.
        | If some modifications are required on the PLC project after being deployed in production, the
        | developer #[bcp14 MUST] create a new Git branch to implement changes. When changes are tested, validated and
        | considered to be put in production, the branch #[bcp14 MUST] be merged into the master branch and a
        | new version will be created in VersionDog.

    section
      name PLC software coding guidelines
      t
        | This chapter provides information on PLC coding guidelines.
      section
        name PLC objects naming convention
        t
          | PLC objects and tag names (for function variables see  #[xref(target="VAR_NAMING")]) #[bcp14 MUST] start with a
          | predefined prefix (see bellow) depending on object type, followed by
          | an underscore #[strong (_)] character and a short exhaustive name written in #[xref(target="camel_case") upper camel case]
          | convention (with first letter capitalized). No spaces are allowed in names.

        table(align='center')
          name PLC objects naming convention
          thead
            tr #[th Type] #[th Prefix] #[th Description]
          tbody
            tr #[td Hardware inputs] #[td I]
              td
                | Hardware inputs symbols #[bcp14 MUST] start with the #[strong I] prefix.
                | There is no distinguish between analog or digital signal types.
            tr #[td Hardware outputs] #[td O]
              td
                | Hardware outputs symbols #[bcp14 MUST] start with the #[strong O] (capital 'o' letter, not zero) prefix.
                | There is no distinguish between analog or digital signal types.
            tr #[td Organization blocks] #[td OB]
              td
                | Organization blocks #[bcp14 MUST] start with the #[strong OB] prefix.
                | For example, a good name for OB1 is #[tt OB_Main].
            tr #[td Functions] #[td FC]
              td
                | Functions #[bcp14 MUST] start with the #[strong FC] prefix.
            tr #[td Functions blocks] #[td FB]
              td
                | Functions blocks #[bcp14 MUST] start with the #[strong FB] prefix.
            tr #[td Data blocks] #[td DB]
              td
                | Data blocks #[bcp14 MUST] start with the #[strong DB] prefix.
                | There is no naming difference between instance data-blocks and global data-blocks.

        t
          | Some examples of valid objects names:
        ul
          li #[strong OB_Main] : This is an organization block, #[tt Main] is probably the main sweep.
          li #[strong I_SwitchIn] : This is a hardware input whose function is #[tt switch in].
          li #[strong O_DrvEnable] : This is a hardware output, #[tt DrvEnable] means for #[tt driver enable] output.
          li #[strong FB_MotorAxis] : This is a function block, probably to drive a motorized axis.

      section
        name PLC hardware configuration guidelines
        t
          | It is #[bcp14 RECOMMENDED] to properly named PLC hardware objects (modules, deported stations, network segments).
          | Name of these objects must be kept short and in #[xref(target="camel_case") camel case] convention.
          | Giving proper names of hardware modules ease the diagnostic and troubleshoting on-site (using the CPU display).
        t
          | If the PLC is connected to the CERN network, the IP configuration #[bcp14 MUST NOT] be set in the PLC project.
          | The #[tt "IP address is set directly at the device"] option #[bcp14 MUST] be selected as shown in this picture:
        figure
          name Proper IP configuration setting
          artwork(align='center' type='binary-art' src='plc-guidelines/ip-configuration.png')
        t
          | Using this technique, we can easily apply IP configuration provided by IT in case of PLC relocation.
          | Another positive point of this option is to use a common PLC project for multiple PLC installations.
        t
          | This requirement #[bcp14 MAY] be ignored whenever this option is not possible,
          | if a UDP, ISO-on-TCP, ISO-Transport or TCP connection is setup, but such cases
          | #[bcp14 SHOULD] be validated.

      section
        name PLC software architecture guidelines
        t
          | To keep the PLC project easy to maintain (new features, bug fixes) and to understand by everyone, some rules
          | #[bcp14 MUST] be followed when creating the software architecture:
        ul
          li
            | #[strong Objects numbering :]
            | Unless required for specific reasons, objects must be automatically numbered by TIA portal. An exception is
            | made for SILECS communication block whose must be located in a specific memory area (by convention, starting at
            | address DB100)
          li
            | #[strong Grouping :]
            | TIA portal can groups several blocks into virtual folders named #[tt Groups]. It is #[bcp14 RECOMMENDED] to create groups
            | to separate every PLC software functionnalities. The #[tt Main organization block] (OB1) #[bcp14 MUST] be kept the #[tt Program blocks]
            | root.
          li
            | #[strong Global objects :]
            | Global objects (such as global DB and mementos) #[bcp14 MUST] be avoided. It breaks the portability of the code and functions.
            | However, some globals objects #[bcp14 SHOULD] be implemented for data exchange (SILECS, external communication)
          li
            | #[strong Functions (FC) :]
            | Functions (not function blocks) #[bcp14 MUST] be short and small function.
          li
            | #[strong Function blocks (FB) :]
            | Function blocks are the most portable and efficient way to implement code in the PLC.
            | This is the #[bcp14 RECOMMENDED] type of blocks to use in the PLC.

      section
        name Coding rules
        t This chapter describes how PLC code #[bcp14 MUST] be written
        section
          name PLC programming language
          t
            | Siemens PLCs can be programmed using different languages (#[tt LAD], #[tt FDB], #[tt STL], #[tt GRAPH], #[tt SCL]).
            | Each programming language has its own advantages, but also drawbacks.
            | Except for the safety part, PLC software #[bcp14 MUST] be written in the #[strong SCL] language, for the following reasons:
          ul
            li
              | Text based : Text based language allows powerful operations like find/replace, regular expressions, easy comparison.
              | copy/paste in other text-based software (chat, notepad...)
            li Compiled : The SLC compiler provided by Siemens shows good results of optimizing code thus reducing the PLC cycle time.
            li Portability : SCL is a compiled software and can be easily moved from a project to another.
            li Comprehensible : A person without PLC experience but with basic programming knowledge can understand the code.

          t
            | Unfortunatly Siemens doesn't provide (for technical reasons, not explained here) a SCL compiler for safety program part (safety PLC only).
            | In this case, the safety PLC program #[bcp14 MUST] be written using the ladder (LAD) software.
            | Siemens made a guideline for safety programming. Document is available #[xref(target="siemens_safety") here].

        section(anchor="VAR_NAMING")
          name Variables naming convention
          t Properly naming variables is the key of a readable software. These rules #[bcp14 MUST] be followed:
          ul
            li Name should be short (less than 20 characters) and #[bcp14 MUST] describe its purpose.
            li #[xref(target="camel_case") lower camel case] convention (with first letter lowercase).
            li Spaces #[bcp14 MUST NOT] be used.
            li Comments metadata must be filled to provide short documentation of variable purpose.
        section(anchor="CONST_NAMING")
          name Constants naming convention
          t
            | Uses of constants in SCL code makes the code more readable and allow easier code modification.
            | These rules #[bcp14 MUST] be followed:
          ul
            li Name #[bcp14 SHOULD] be short (less than 20 characters) and #[bcp14 MUST] describe its purpose.
            li Name #[bcp14 MUST] be in uppercase, using the #[xref(target="snake_case") snake case] convention.
            li Comments metadata #[bcp14 MUST] be filled to provide short documentation of constant purpose.
        section(anchor="FC_CODING")
          name Functions
          t
            | Functions code #[bcp14 MUST] be short and small.
            | It #[bcp14 MUST NOT] access global data-blocks, mementos or hardware I/O.
            | Input, outputs, inputs/outputs and return value of these functions are the only way to exchange data.
            | All these parameters #[bcp14 MUST] have a detailled description in the #[tt Comment] field.
        section(anchor="FB_CODING")
          name Function blocks
          t
            | With their associated static data, a #[tt FB] is close to object programming.
          t
            | The static data of a #[tt FB] can host other child #[tt instance DB], so a real (and portable) hierarchy is achieved.

          t
            | #[tt Function blocks] attributes #[bcp14 SHOULD] be configured with #[tt Optimized block access] (this is the case by default).
            | Enabling the #[tt Optimized block access] attribute disables direct addressing and removes some TIA portal memory optimization.
            | A common usage requiring to disable #[tt Optimized block access] is to use the #[tt AT] operator for 'casting' data structures.

          t
            | The #[tt AT] operator was used in some old PLC projects to decode bit fields and #[tt MUST] be replaced by calls to #[tt GATHER] and/or #[tt SCATTER]
            | built-in functions.
          t
            | As for #[xref(target="FC_CODING") functions], the code inside #[tt FB] #[bcp14 MUST NOT] access global data-blocks, mementos or hardware I/O.
            | Static variables can be read/write outside #[tt FB] scopre, however it is #[bcp14 RECOMMENDED] to use inputs,
            | inputs/outputs and outputs parameters of the #[tt FB].

          t
            | Each #[tt function block] #[bcp14 SHOULD] implement a process functionnality, it #[bcp14 MUST] also keep track of its own state
            | by making use of its static variables (see #[xref(target="state_machine")]).

          section(anchor="state_machine")
            name State machine
            t
              | To reduce PLC cycle time and avoid unnecessary PLC computations, implementing a state machine for the process is #[bcp14 RECOMMENDED].
            t
              | State machines can easily be implemented in SCL using an #[tt int] static variable and a #[tt CASE] statement.
            t
              | Known states #[bcp14 MUST] be declared as #[tt CONSTANT FB] values. By using constants, it's easy to add or remove new states from the state machine
              | without renumbering all steps.
            t
              | A default (#[tt ELSE]) statement  #[bcp14 SHOULD] be implemented to
              | fallback to a known state (or to a fault state, depending on project requirements).
              | This special case is useful if the variable is forced to a non-valid value during development and commissioning.
              | Here is a short example of how to implement a state machine in SCL:
            figure
              name Implementing a state machine in SCL.
              sourcecode
                | FUNCTION_BLOCK "FB_LightControl"
                | { S7_Optimized_Access := 'TRUE' }
                | VERSION : 0.1
                |   VAR_INPUT
                |       pushButton : Bool;   // Push button input (True : button pressed)
                |   END_VAR
                |
                |   VAR_OUTPUT
                |       lightRelay : Bool;   // Light relay output
                |   END_VAR
                |
                |   VAR
                |       state : Int;   // State machine
                |       nbPush : Int;   // Number of push detected
                |       R_Trig {InstructionName := 'R_TRIG'; LibVersion := '1.0'} : R_TRIG;   // Rising edge detector
                |   END_VAR
                |
                |   VAR_TEMP
                |       risingEdge : Bool;   // Is rising edge detected
                |   END_VAR
                |
                |   VAR CONSTANT
                |       STATE_IDLE : Int := 0;   // Idle state, waits for user input
                |       STATE_ON : Int := 1;   // On state
                |       NB_PUSH_ON : Int := 4;   // Number of pushes before switching on
                |       NB_PUSH_OFF : Int := 2;   // Number of pushes before switching off
                |   END_VAR
                |
                | BEGIN
                |   (*
                |       Simple state machine example
                |       It waits for 4 users button press, switch the relay on
                |       then waits for 2 button presses then switch relay off
                |   *)
                |   #R_Trig(CLK:=#pushButton,
                |           Q=>#risingEdge);
                |
                |   CASE #state OF
                |       #STATE_IDLE:
                |           //Waits for button to be pressed
                |           IF (#risingEdge = TRUE) THEN
                |               #nbPush := #nbPush + 1;
                |               IF (#nbPush >= #NB_PUSH_ON) THEN
                |                   #lightRelay := TRUE;
                |                   #nbPush := 0;
                |                   #state := #STATE_ON;
                |               END_IF;
                |           ELSE
                |               #lightRelay := FALSE;
                |           END_IF;
                |       #STATE_ON:
                |           IF (#risingEdge = TRUE) THEN
                |               #nbPush := #nbPush + 1;
                |               IF (#nbPush >= #NB_PUSH_OFF) THEN
                |                   #nbPush := 0;
                |                   #lightRelay := FALSE;
                |                   #state := #STATE_IDLE;
                |               END_IF;
                |           END_IF;
                |       ELSE  // Unhandled state
                |           #state := #STATE_IDLE;
                |   END_CASE;
                |
                | END_FUNCTION_BLOCK
          section(anchor="timers")
            name Timers
            t
              | With S7-1500 family, timers functions (TP, TON, TOF, TONR) are not anymore linked to a hardware timer number.
              | These functions #[bcp14 MUST] be used for all timer related operations.
            t
              | However, on previous CPU generation (S7-300 and S7-400), timers were linked to a timer number, making the
              | code unportable. To avoid this problem, a #[tt function block] #[tt FB_Timer] was created in the past.
            t
              | This function implemented a 0.1s accuracy timer by counting rising edge of a clock memento bit.
            t
              | This document will not cover the usage and implementation of this function but the PLC developer #[bcp14 MUST]
              | use this function if he has to develop S7-300 or S7-400 PLC software.

      section
        name Code review
        t
          | Before deploying PLC code to an operational equipment, the PLC implementation #[bcp14 MUST] be reviewed by
          | section's PLC experts for approval.

        t
          | This code review exercise is the opportunity to discover potential issues
          | and ensure the code respects standards defined in this document.

        t
          | The code review goal #[bcp14 MUST NOT] be to blame the PLC developer, it is a moment to improve common knowledge about PLC
          | technologies, additional reviews and or support #[bcp14 MAY] be requested to PLC experts (and is welcomed).


    section
      name Contributors
      ul
        li Sylvain Fargier (BE-CEM-MRO)

  back
    references
      name References
      references
        name Normative References
        | &rfc2119;

      references
        name Informative References

      references
        name URL References
        reference(anchor='git' target='https://git-scm.com')
          front
            title Git distributed version control system
            author
              organization Git community

        reference(anchor='git_repo' target='https://gitlab.cern.ch/mro/controls/plc')
          front
            title MRO PLC Git repository
            author
              organization CERN

        reference(anchor='cern_versiondog' target='https://readthedocs.web.cern.ch/display/ICKB/VersionDog')
          front
            title CERN VersionDog support
            author
              organization CERN

        reference(anchor='siemens_safety' target='https://assets.new.siemens.com/siemens/assets/api/uuid:1a7ce755-79a1-494e-ae14-5e5f72c29205/safety-programming-guideline-for-simatic-s7-1200-and-1500.pdf')
          front
            title Siemens safety programming guidelines
            author
              organization Siemens

        reference(anchor='camel_case' target='https://en.wikipedia.org/wiki/Camel_case')
          front
            title Camel-case definition
            author
              organization Wikipedia

        reference(anchor='snake_case' target='https://en.wikipedia.org/wiki/Snake_case')
          front
            title Snake-case definition
            author
              organization Wikipedia

    section
      name Change Log
      t A list of changes

    section
      name Open Issues
      t A list of open issues regarding this document
