namespace xlink = "http://www.w3.org/1999/xlink"
default namespace = "http://www.cern.ch/ssvg"

grammar {

start = element state {
  # default transition for relations
  element transition { Transition } ?,
  (
    element property { Property } |
    element computed { Computed }
  ) *
}

Property =
  attribute name { text },
  ((
    attribute type { "boolean" }
  ) | (
    attribute type { "number" },
    # defaults to [ 0, 1 ]
    attribute min { xsd:double } ?,
    attribute max { xsd:double } ?
  ) | (
    attribute type { "auto" },
    # defaults to [ 0, 1 ]
    attribute min { xsd:double } ?,
    attribute max { xsd:double } ?
  ) | (
    attribute type { "enumerated" },
    attribute values { ValueList }
  )),
  (
    element relation { Relation } |
    element transform { Transform } |
    element direct { DirectRelation }
  ) *

Computed =
  Property,
  # along with an "update" DOM attribute
  attribute onupdate { Script }

DirectRelation =
  attribute query-selector { text },
  attribute attribute-name { text } ?,
  attribute attribute-type { text } ?,
  attribute onupdate { Script } ?,
  element transition { Transition } *

Relation =
  DirectRelation,
  attribute calc-mode { "discrete" | "linear" } ?,
  attribute key-times { KeyTimesList } ?,
  ((
    attribute from { text } ?,
    attribute to { text }
  ) | (
    attribute by { text }
  ) | (
    attribute values { ValueList },
  ))

Transform = Relation

# Use EBNF to describe those types
ValueList = text
KeyTimesList = text

Transition =
  attribute duration { DurationValue } ?,
  attribute timing-function {
    "linear" | "ease" | "ease-in" | "ease-out" | "ease-in-out" |
    xsd:string { pattern = "cubic-bezier((\d{1,}(\.\d+)?|\.\d+)(,(\d{1,}(\.\d+)?|\.\d+)){3})" }
  } ?,
  attribute delay { DurationValue } ?,
  attribute type { "direct" | "strict" } ?

DurationValue = xsd:string { pattern = "(\d{1,}(\.\d+)?|\.\d+)(s|ms)?" }
Script = text

}
