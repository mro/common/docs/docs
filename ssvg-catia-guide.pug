<?xml version='1.0' encoding='utf-8'?>
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
<!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
// local reference to another doc
| ]>


// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>


// categories: std | bcp | info | exp | historic
rfc(category='bcp' consensus='false' ipr='trust200902' docName='ssvg-catia-guide' submissionType='be-cem-mro' xml:lang='en' version='3' xmlns:xi="http://www.w3.org/2001/XInclude")
  front
    title Creating SSVGs from 3D models with CATIA Composer
    author(fullname='Gabriele De Blasi' initials='G.D.B.' role='editor' surname='De Blasi')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email gabriele.deblasi@cern.ch
    date(year='2023' month='June')

    keyword CATIA
    keyword SSVG
    keyword CAD

    abstract
      t
        | This guide explains how to generate a SSVG (Stateful SVG)
        | document from a 3D model using Catia Composer.
  middle

    section
      name Introduction
      t
        | At CERN there are various 3D models of devices or systems that
        | can be animated and used to visualize an actual device state,
        | such as the position of a component.
        | In this document, we will first explain how to do the environemnt
        | setup and then how to convert such a model into a SSVG
        | #[xref(target="STATEFUL_SVG") Stateful SVG] document using
        | #[xref(target="CATIA_COMPOSER") Catia Composer] and
        | #[xref(target="SSVG_EDITOR_APP") SSVG Editor].

    section
      name Requirements Notation
      t
        | The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
        | "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
        | document are to be interpreted as described in BCP 14,
        | #[xref(target='RFC2119' format='default') RFC2119].

    section(anchor="SEC_ENV_SETUP")
      name Environment set-up
      t
        | To get #[xref(target="CATIA_COMPOSER") Catia Composer] at CERN it
        | is required:
      ol
        li
          | A physical or virtual machine with #[tt Windows 10 Pro (64 bit)] or
          | #[tt Windows 7 Enterprise SP1 (64 bit)]
          | (see #[xref(target="CATIA_FAQ_BP") CATIA FAQ / BEST PRACTICES])
        li
          t
            | To submit a request to the
            | #[xref(target="CAD_SERVICE_REQUEST") CATIA Support team]
            | via the CERN Service Portal, by entering in:
          ul
            li #[strong Short description]: Catia Composer installation
            li
              t
                | #[strong Detailed description]:
                | Request to add the machine "&lt;name machine&gt;" to the
                | "CATIA unsupported" NSC group and to enable the Catia Composer
                | installation in order to extract SVGs from Step files (.stp).
              t
                | #[strong NOTE]: the &lt;name machine&gt; is the name of your
                | Windows machine corresponding to the "Device Name" in
                | #[em Windows Settings &gt; System &gt; About].
            li Other fields may be skipped
        li
          t
            | To install it via the CERN CMF installation manager once the
            | request has been fulfilled and the machine has been enabled
            | to install Catia Composer, as shown in the figure below:
          figure
            name CFM packages installation
            artwork(align='center' type='binary-art' src='ssvg-catia-guide/cmf_installation.png')

          t
            | #[strong NOTE]: it is mandatory to have admin rights on the
            | machine to install packages.

    section
      name From 3D model to SSVG

      t In this section, we will go through the steps to generate an SSVG.

      section
        name SVG generation

        section
          name Open 3D model

          t
            | Assuming CATIA Composer tool is installed and running
            | (see #[xref(target="SEC_ENV_SETUP")] otherwise), open the file
            | containing the model to be converted into an SVG
            | (e.g. #[em .stp], #[em .smg], #[em .mod], etc.):
          figure
              name Open model
              artwork(align='center' type='binary-art' src='ssvg-catia-guide/catia_open_step_file.png')

        section
          name Change point of view

          t
            | At first, the camera #[em point of view] (or #[em perspective])
            | #[bcp14 MUST] be configured, namely the position from which an
            | observer should see the model.
            | To this end, Catia Composer offers several ways to move the
            | camera viewpoint:
          ul
            li
              t #[strong pan], translate the model in any direction:
              ul
                li
                  | by holding down the mouse scroll wheel button and moving
                  | the mouse around;
                li
                  | by holding down the CTRL key and using the directional
                  | arrows on the keyboard;
                li
                  | or, by selecting the #[em pan mode] from the HOME tab in
                  | the ribbon on top (see #[xref(target="FIG_RIBBON_HOME")]).
            li
              t
                | #[strong rotate], rotate the model around a pivot point:
              ul
                li
                  | by holding down the right mouse button in the desired pivot
                  | point and moving the mouse around;
                li
                  t
                    | by holding down the SHIFT key and using the directional
                    | arrows on the keyboard.
                  t
                    | #[strong NOTE]: in this case the pivot point is the last
                    | one selected with the right mouse button;
                li
                  | or, by selecting the #[em rotate mode] from the HOME tab in the
                  | ribbon on top (see #[xref(target="FIG_RIBBON_HOME")]).
            li
              t #[strong zoom], raise/lower the magnification of viewpoint:
              ul
                li
                  | by rolling the mouse wheel forward/backward;
                li
                  | or, by selecting the #[em zoom mode] from the HOME tab in the
                  | ribbon on top (see #[xref(target="FIG_RIBBON_HOME")]).

          figure(anchor="FIG_RIBBON_HOME")
            name Ribbon HOME tab - Catia Composer
            artwork(align='center' type='binary-art' src='ssvg-catia-guide/catia_ribbon_home_tab.png')

          t
            | #[strong NOTE]: the settings about the camera viewpoint can be
            | modified in #[em File &gt; Preferences &gt; Camera].

        section
          name Create view

          t
            | Once the camera is positioned, it is  #[bcp14 RECOMMENDED]
            | to save that point of view to be used as a starting
            | point for subsequent modifications (checkpoint).
            | To do it, go inside the Views tab and click on the
            | flashing camera icon as shown in #[xref(target="FIG_CREATE_VIEW")].
          t
            | #[strong NOTE]: creating several views while making changes
            | #[bcp14 SHOULD] help to save time since, in case of errors,
            | it is possible to resume from a given view even after closing
            | Catia Composer.
            | Obviously, do not forget to save before closing the tool.

          figure(anchor="FIG_CREATE_VIEW")
            name Create view - Catia Composer
            artwork(align='center' type='binary-art' src='ssvg-catia-guide/catia_create_view.png')

        section
          name Change model display settings

          t
            | This subsection is intended to illustrate some settings that
            | #[bcp14 MAY] be changed to improve the appearance of the model.

          section
            name Rendering

            t
              | Exploring the RENDER tab of the ribbon on top, there are
              | many options to enable/disable in order to adjust the look of
              | the model; amongst them, the render mode (#[xref(target="FIG_RENDER_MODE")]).
              | The one used for the generation of the SVGs in this guide is
              | #[strong Flat Technical], which emphasises the outlines.

            figure(anchor="FIG_RENDER_MODE")
              name Render mode - Catia Composer
              artwork(align='center' type='binary-art' src='ssvg-catia-guide/catia_render_mode.png')

          section
            name Perspective

            t
              | The camera perspective #[bcp14 MAY] be deactivated (removing
              | the impression of "depth") by toggling the relevant button
              | in the footer (#[xref(target="FIG_DISABLE_PERSPECTIVE")]).
              | See the difference by comparing the
              | #[xref(target="FIG_RENDER_MODE")] and
              | #[xref(target="FIG_DISABLE_PERSPECTIVE")].

            figure(anchor="FIG_DISABLE_PERSPECTIVE")
              name Disable camera perspective - Catia Composer
              artwork(align='center' type='binary-art' src='ssvg-catia-guide/catia_disable_perspective.png')


        section(anchor="SEC_HIDE_UNWANTED_ELEMENTS")
          name Hide unwanted elements

          t
            | It may happen that one wants to only show some parts of a 3D
            | model, hiding unwanted components. To achieve this:
          ul
            li
              | #[strong hide elements one by one], seleting an element
              | each time and pressing the H key on the keyboard;
            li
              | #[strong hide elements in group], selecting them through
              | the classic rectangular selection area (drawn by holding down
              | the left mouse button) or the combination of CTRL key and
              | left-click, and then pressing the H key on the keyboard.

          t
            | The hidden elements (called actors in Catia Composer) are
            | displayed in the #[em Assembly] tab with unchecked boxes
            | (see #[xref(target="FIG_HIDDEN_COMPONENTS")]).
            | So, to make hidden elements reappear, the relative checkboxes
            | have to be ticked again. To quickly make all elements
            | reappear, use the keyboard shortcut 'CTRL + T'.

          figure(anchor="FIG_HIDDEN_COMPONENTS")
            name 3D model with hidden components - Catia Composer
            artwork(align='center' type='binary-art' src='ssvg-catia-guide/catia_hidden_components.png')

        section
          name Generate SVGs

          t
            | At this stage, everything is ready to generate an SVG.
          t
            | So, go in the HOME tab of the ribbon on top and select
            | #[em Technical Illustration]. A #[em Workshops] panel should
            | appear through which it is possible to generate an SVG.
            | But before that, the vectorisation settings #[bcp14 SHOULD]
            | be fine-tuned.
            | Also, it is #[bcp14 RECOMMENDED] to save all those settings
            | creating a new profile.
          t
            | Finally, the SVG document can be generated:
          ul
            li
              | clicking on the #[strong Preview] button opens the default
              | browser that displays the generated SVG inside an html page;
            li
              t
                | clicking on the #[strong Save as] button opens a dialog window
                | for saving the SVG doc.
              t #[strong NOTE]: do not forget to set #[em SVG] as type

          figure(anchor="FIG_SAVE_AS_SVG")
            name Save SVG - Catia Composer
            artwork(align='center' type='binary-art' src='ssvg-catia-guide/catia_save_as_svg.png')

      section
        name Multi-layered SVG

        t
          | When exporting a view as SVG, Catia Composer delivers an SVG
          | document in which the graphic elements are not well separated
          | and therefore difficult to manipulate. In addition, non-visible parts
          | are cropped as shown below, trying to move the outermost quadrant
          | to the top:

        figure(anchor="FIG_SVG_CROPPED_ELEMENTS")
          name Cropped elements by editing SVG
          artwork(align='center' type='binary-art' src='ssvg-catia-guide/svg_cropped_elements.png')

        t
          | Special care #[bcp14 MUST] be brought to handle cases where
          | such elements are to be manipulated or moved by SSVG.

        t
          | #[strong NOTE:] This section #[bcp14 MAY] be skipped for documents
          | where all parts #[bcp14 SHOULD] be visible.

        t
          | At first, the movable parts #[bcp14 MUST] be identified to
          | create the SVGs that will represent the different layers.
          | Each moving element #[bcp14 SHOULD] have its own layer, but
          | static layers #[bcp14 MAY] also be needed.

        t
          | In our example, the following six layers have been identified
          | to animate the movable elements:

        figure(anchor="FIG_MULTI_LAYERED_SVG")
            name Multi-Layered SVG
            artwork(align='center' type='binary-art' src='ssvg-catia-guide/multilayered_svg.png')

        t
          | In detail, the layer 3 represents the vertical support for the
          | three quadrants on the left (layers 1, 5 and 6) and can move
          | horizontally along the base (layer 4).
          | The quadrants, on the other hand, can move vertically along the
          | vertical support (layer 3). The remaining layer 2, on which two
          | limit switches are installed, serves only to cover the rails of
          | the vertical support.

        t
          | #[strong NOTE]: the extraction of those layers has been performed
          | by hiding the unwanted elements as explained in
          | #[xref(target="SEC_HIDE_UNWANTED_ELEMENTS")] and
          | keeping the point of view unchanged.


        t
          | Combined in the exact order the layers return the initial model.
          | The SVGs combination can be performed by merging the contents of
          | all SVGs into a single one, remembering that the rendering
          | order in a SVG document follows a simple rule
          | (#[xref(target="SVG11_RENDERING_ORDER")] SVG Rendering Order):
          |
          em
            | "First elements getting painted first. Subsequent elements are
            | painted on top of previously painted elements".
        t
          | Consequently, the layers closest to the observer #[bcp14 MUST] be
          | the last elements and vice versa. Continuing the example with the
          | layers mentioned above, the merging should be something like:

        figure(anchor="FIG_COMBINATION_SVG_FILES")
            name SVGs combination
            artwork(align='center' type='binary-art' src='ssvg-catia-guide/combination_svg_files.png')

        t
          | #[strong NOTE]: it is strongly #[bcp14 RECOMMENDED] to assign
          | appropriate ids and class names to the SVG elements to be animated.
          | This will simplify the next step.
      section
        name SSVG creation

        t
          | The last step consists of adding a logical state to the SVG document.
          | To do that, do refer to the
          | #[xref(target="SSVG_EDITOR_USER_GUIDE") SSVG Editor User Guide].

        t
          | The SSVG created from the 3D model under consideration is
          | available #[xref(target="CHARM_XZ_TABLE_SSVG") here].

    section
      name Contributors
      ul
        li Sylvain Fargier (BE-CEM-MRO)

  back
    references
      name References

      references
        name Normative References
        | &rfc2119;

        reference(anchor='SVG11_RENDERING_ORDER' target='https://www.w3.org/TR/SVG11/render.html#RenderingOrder')
          front
            title SVG Rendering Order
            author
              organization W3C

      references
        name CERN Sites and Repositories

        reference(anchor='SSVG_EDITOR_APP' target='https://mro-dev.web.cern.ch/ssvg-editor')
          front
            title SSVG-Editor
            author(fullname="Sylvain Fargier")
              organization CERN

        reference(anchor='CAD_SERVICE_REQUEST' target='https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=CAD-service')
          front
            title CAD Service request
            author
              organization CERN

        reference(anchor='CHARM_XZ_TABLE_SSVG' target='https://gitlab.cern.ch/mro/controls/interfaces/www/charm-devices/-/blob/master/www/dist/img/XZ-Table.svg')
          front
            title CHARM XZ Table SSVG
            author(fullname="Gabriele De_Blasi" surname="De Blasi")
              organization CERN

      references
        name CERN Documentations and Specifications

        reference(anchor='CATIA_FAQ_BP' target='https://edms.cern.ch/ui/file/1531362/LAST_RELEASED/1531362.pdf')
          front
            title CATIA FAQ / BEST PRACTICES
            author(fullname="Benoit Lepoittevin")
              organization CERN
          refcontent EDMS 1531362

        reference(anchor='SSVG_EDITOR_USER_GUIDE' target='https://mro-dev.web.cern.ch/docs/bcp/ssvg-editor-user-guide.html')
          front
            title SSVG-Editor User-guide
            author(fullname="Sylvain Fargier")
              organization CERN
          refcontent EDMS 2820020

        reference(anchor='STATEFUL_SVG' target='https://mro-dev.web.cern.ch/docs/std/ssvg-specification.html')
          front
            title Stateful Scalar Vector Graphics Specification
            author(fullname="Sylvain Fargier")
              organization CERN
          refcontent EDMS 2417668

      references
        name Others

        reference(anchor='CATIA_COMPOSER' target='https://www.3ds.com/products-services/catia/products/composer/portfolio/composer')
          front
            title CATIA Composer
            author
              organization Dassault Aviation

