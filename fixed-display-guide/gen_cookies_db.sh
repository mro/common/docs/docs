#!/bin/bash

FILE="$1"

die() {
  echo $1 >&2
  exit 1
}

rm -f cookies.sqlite
echo "CREATE TABLE moz_cookies (id INTEGER PRIMARY KEY, originAttributes TEXT NOT NULL DEFAULT '', name TEXT, value TEXT, host TEXT, path TEXT, expiry INTEGER, lastAccessed INTEGER, creationTime INTEGER, isSecure INTEGER, isHttpOnly INTEGER, inBrowserElement INTEGER DEFAULT 0, sameSite INTEGER DEFAULT 0, rawSameSite INTEGER DEFAULT 0, schemeMap INTEGER DEFAULT 0, CONSTRAINT moz_uniqueid UNIQUE (name, host, path, originAttributes));" | sqlite3 cookies.sqlite || die "failed to create"

COOKIES=$(cat $FILE | grep -v -e '^\(# .*\)\?$')
IFS="
"
for c in $COOKIES; do
  C_DOMAIN=$(echo "$c" | cut -f 1)
  C_FLAG=$(echo "$c" | cut -f 2)
  [ "$C_FLAG" == "TRUE" ] && C_FLAG=1 || C_FLAG=0
  C_PATH=$(echo "$c" | cut -f 3)
  C_SECURE=$(echo "$c" | cut -f 4)
  [ "$C_SECURE" == "TRUE" ] && C_SECURE=1 || C_SECURE=0
  C_EXPIRATION=$(echo "$c" | cut -f 5)
  C_NAME=$(echo "$c" | cut -f 6)
  C_VALUE=$(echo "$c" | cut -f 7)
  NOW=$(date +%s)
  echo "Inserting ${C_NAME}"
  echo "INSERT INTO moz_cookies (name,value,host,path,expiry,lastAccessed,creationTime,isSecure,isHttpOnly,schemeMap) VALUES('$C_NAME', '$C_VALUE', '$C_DOMAIN', '$C_PATH', $C_EXPIRATION, $NOW, $NOW, $C_SECURE, $C_FLAG, 2);" | sqlite3 cookies.sqlite || die "failed to insert"

done
