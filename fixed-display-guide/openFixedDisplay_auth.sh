SCRIPT_PATH=$(cd $(dirname $0); pwd)

die() {
  echo $1 >&2
  exit 1
}

echo "Authenticating kerberos: ${USER}"
kinit -k -t ~/.keytab "${USER}" || die "kinit failed"

echo "Generating cookies"
rm -f cookies.txt
auth-get-sso-cookie -u "$URL" -o cookies.txt -v -vv || die "failed to generate cookies"

echo "Converting cookies"
${SCRIPT_PATH}/gen-cookies-db "cookies.txt" || die "failed to generate firefox cookie db"

gen_profile() {
  echo "Starting headless firefox to create profile"
  firefox --profile ~/.mozilla/firefox/app1 --headless &
  P=$!
  while [ ! -e ~/.mozilla/firefox/app1/cookies.sqlite ]; do
    echo "Waiting for firefox to create profile ..."
    sleep 1
  done
  kill -TERM "$P"
  sleep 2
  kill -KILL "$P"
}

mv cookies.sqlite ~/.mozilla/firefox/app1/cookies.sqlite || die "failed to move cookies"
