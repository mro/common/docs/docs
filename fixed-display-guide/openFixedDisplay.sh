#!/bin/bash

URL='<url>'

# Firefox variables
FF_PROFILE_PATH="~/.mozilla/firefox/app1"
FF_OPTS="--kiosk --profile ${FF_PROFILE_PATH} --new-instance"

firefox --help | grep -q start-debugger-server > /dev/null
FF_HAS_DEBUG_SERVER=$?

# remove firefox profile
rm -rf $FF_PROFILE_PATH
mkdir -p $FF_PROFILE_PATH

if [ $FF_HAS_DEBUG_SERVER -eq 0 ]
then
  # Enable the remote debugging
  echo "Enabling remote debugging"
  echo 'user_pref("devtools.debugger.remote-enabled", true);
  user_pref("devtools.chrome.enabled", true);
  user_pref("devtools.debugger.prompt-connection", false);' >> $FF_PROFILE_PATH/user.js

  FF_OPTS="${FF_OPTS} --start-debugger-server 6000"
else
  echo "Remote debugging not available"
fi

echo "Disabling screensaver"
gsettings set org.gnome.desktop.screensaver lock-enabled false
gsettings set org.gnome.desktop.screensaver idle-activation-enabled false
gsettings set org.gnome.desktop.session idle-delay 0

echo "Starting app"
# start firefox with nohup as first istance. kiosk mode is fullscreen mode
nohup firefox $FF_OPTS "$URL" > /dev/null 2>&1 &
sleep 2

# hide mouse cursor
xdotool mousemove 0 0
xdotool mousemove_relative 10000 10000
xdotool click 1
