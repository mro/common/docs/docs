<?xml version='1.0' encoding='utf-8'?>
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
<!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
// local reference to another doc
| ]>


// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>


// categories: std | bcp | info | exp | historic
rfc(category='bcp' consensus='false' ipr='trust200902' docName='fixed-display-guide' submissionType='be-cem-mro' xml:lang='en' version='3' xmlns:xi="http://www.w3.org/2001/XInclude")
  front
    title Fixed-display web app deployment
    author(fullname='Gabriele De Blasi' initials='G.D.B.' role='editor' surname='De Blasi')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email gabriele.deblasi@cern.ch
    date(year='2022' month='September')

    keyword fixed-display
    keyword fixed-screen
    keyword dashboard

    abstract
      t
        | This document is intended as a guide for the display of web
        | applications on fixed monitors.
  middle

    section
      name Introduction
      t
        | This guide will help you display web applications on fixed monitors,
        | like dashboards.
        | Although it has been tested on CERN CentOS 7 (CC7) and CentOS Stream
        | 8 (CS8), it can be used with other Linux operating systems.

      t
        | Example scripts that will be presented in this guide can be found
        | #[xref(target="VUE_SITE_SHOW_SCRIPTS") here],
    section
      name Requirements Notation
      t
        | The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
        | "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
        | document are to be interpreted as described in BCP 14,
        | #[xref(target='RFC2119' format='default') RFC2119].

    section
      name Displaying a WebApp
      t Here is a step-by-step procedure to achieve the goal:

      section
        name Machine set-up
        t
          | If your machine already has a Linux OS, you can think about skipping
          | this step; otherwise, please refer to
          | #[xref(target="LINUX_INSTALLATION") Linux@CERN installation]
          | for installing one of the CERN supported Linux distributions.

      section
        name Service account and autologin
        t
          | It is #[bcp14 RECOMMENDED] to use a CERN service account instead of
          | a personal one as a Linux User account and for any authentication
          | required by the application.
          | If you need one, you can create it via the link
          | #[xref(target="SERVICE_ACCOUNT") Service Account].
        t
          | It is #[bcp14 RECOMMENDED] to use #[strong mroproddisplays]
          | for fixed-display applications.

        t
          | Once you have a service account, add it to the Linux machine by
          | running this command:
        sourcecode(type='bash')
          | adduser &lt;username&gt;
        t And then log-in at least once with its credentials.

        t
          | Furthermore, you can enable the automatic login editing the GDM
          | custom configuration file - usually located at
          | #[em /etc/gdm/custom.conf] - by adding the following two lines
          | under the #[strong daemon] section:
        sourcecode(type='bash')
          | [daemon]
          | ...
          | AutomaticLoginEnable=True
          | AutomaticLogin=&lt;username&gt;

      section
        name Automatic launch of the web application
        t
          | This sub-section illustrates how to automatically open and display
          | a web application at start-up.

        t
          | Make sure the following tools are installed first:
        ul
          li Firefox v71 onwards (kiosk mode needed)
          li xdotool

        t
          | Then, create the #[strong openFixedDisplay.sh] file in
          | #[em ~/scripts], copy the script below into it and replace
          | #[strong &lt;url&gt;] with the url of the application to be shown.

        sourcecode(type='bash' src='fixed-display-guide/openFixedDisplay.sh')
        t
          | #[strong Note]: A dedicated Firefox profile is #[bcp14 RECOMMENDED]
          | to keep all application-related information separate from the
          | others.

        t
          | #[strong Note]: The illustrated script enables the
          | #[xref(target="REMOTE_DEBUGGING") remote debugging] if supported
          | by the version of Firefox installed.

        t
          | At this point, to execute that script automatically at boot time,
          | you need to create the #[strong fixed_display.desktop] file in the
          | #[em ~/.config/autostart] folder:
        sourcecode(type='bash')
          | [Desktop Entry]
          | Name=Fixed Display Autostart
          | GenericName=Fixed Display Autostart
          | Comment=This script opens &lt;appname&gt;
          | Exec=/home/&lt;username&gt;/scripts/openFixedDisplay.sh
          | Terminal=false
          | Type=Application
          | X-GNOME-Autostart-enabled=true
        t where #[em &lt;username&gt;] is the one used so far.

        t
          | Finally, the script file #[bcp14 MUST] be executable:
        sourcecode(type='bash')
          | chmod +x ~/scripts/openFixedDisplay.sh

      section
        name Kerberos authentication
        t
          | Some web applications require authentication to access them.
          | If that is not the case, ignore this step. Conversely, the use of
          | Kerberos is recommended.

        t
          | To achieve this, you first have to store kerberos credentials in a
          | Kerberos keytab file by using #[strong ktutil] and replacing the
          | #[em &lt;username&gt;] placeholder properly:
        sourcecode(type='bash')
          | # run 'ktutil' in your home directory
          | ktutil
          |
          | # inside 'ktutil' execute these commands:
          | ktutil:  add_entry -password -p &lt;username&gt; -k 1 -e rc4-hmac
          | ktutil:  write_kt .keytab
          | ktutil:  exit
        t
          | #[strong Note]: Remember to update the keytab file each time
          | Kerberos credentials are modified.

        t
          | The Kerberos credentials allow you to get the CERN Single Sign-On
          | (SSO) cookie through the utility
          | #[xref(target="AUTH_GET_SSO_COOKIE") auth-get-sso-cookie],
          | which in turn is used to access the protected URL.

        t
          | If that tool is not present in the system, copy and paste the
          | following in a new #[em repo] file (or in an existing one) within
          | #[em /etc/yum.repos.d]:
        sourcecode(type='bash')
          | [auth-cern-sso]
          | name=auth cern sso
          | baseurl=http://linuxsoft.cern.ch/internal/repos/authz7-stable/x86_64/os/
          | enabled=1
          | gpgcheck=0
        t and then install it:
        sourcecode(type='bash')
          | sudo yum install auth-get-sso-cookie

        t
          | Then, to carry out authentication, you have to:
        ul
          li obtain and cache Kerberos ticket;
          li get and store CERN SSO cookies;
          li convert cookies from plain text to SQLite table;
          li inject the latter into the Firefox profile.
        t
          | Hence, insert the piece of code below in
          | #[em ~/scripts/openFixedDisplay.sh], just before starting the app:
        sourcecode(type='bash' src='fixed-display-guide/openFixedDisplay_auth.sh')

        t
          | #[strong Note]: to inject cookies into a newly generated profile,
          | it is necessary to first close Firefox.

        t
          | Concerning cookie convertion, it is performed in the separate script
          | #[strong gen-cookies-db] to be located in the same directory, namely
          | #[em ~/scripts]:
        sourcecode(type='bash' src='fixed-display-guide/gen_cookies_db.sh')
        t Do not forget to make it executable:
        sourcecode(type='bash')
          | chmod +x ~/scripts/gen-cookies-db

    section
      name Power-Saving mode
      t
        | There are probably times when screens are not used at all and it
        | would therefore be wise to switch them off.
      t
        | Beyond the reduction on power consumption, scheduling the monitors'
        | power on and off may also prevents so-called
        | "Image Burn-In/Image Retention" effects due to stationary images on a
        | screen.

      t
        | One way to power-on and power-off a screen is to run the
        | #[strong xset] utility throught two #[em systemd] services
        | (#[strong dpms-on.service] and #[strong dpms-off.service])
        | and trigger these latter using the relevant #[em systemd] timers
        | (#[strong dpms-on.timer] and #[strong dpms-off.timer]).

      t
        | The files to be placed in #[em /etc/systemd/system/] directory are:

      figure
        name dpms-on.service
        sourcecode(type="bash")
          | [Unit]
          | Description=DPMS ON
          |
          | [Service]
          | Environment=DISPLAY=:0
          | ExecStart=/usr/bin/xset dpms force on
          | User=&lt;username&gt;
          | Group=wheel

      figure
        name dpms-on.timer
        sourcecode(type="bash")
          | [Unit]
          | Description=DPMS ON timer
          |
          | [Timer]
          | Unit=dpms-on.service
          | OnCalendar=&lt;time&gt;
          | AccuracySec=30s
          | Persistent=false
          |
          | [Install]
          | WantedBy=timers.target

      figure
        name dpms-off.service
        sourcecode(type="bash")
          | [Unit]
          | Description=DPMS OFF
          |
          | [Service]
          | Environment=DISPLAY=:0
          | ExecStart=/usr/bin/xset dpms force off
          | User=&lt;username&gt;
          | Group=wheel

      figure
        name dpms-off.timer
        sourcecode(type="bash")
          | [Unit]
          | Description=DPMS OFF timer
          |
          | [Timer]
          | Unit=dpms-off.service
          | OnCalendar=&lt;time&gt;
          | AccuracySec=30s
          | Persistent=false
          |
          | [Install]
          | WantedBy=timers.target

      t
        | The #[strong &lt;username&gt;], again, corresponds to the one used
        | throughout the document and #[strong &lt;time&gt;] represents a
        | time and/or date specification like #[em Mon-Fri, 06:30:00]. For more
        | information about the time and date specifications see the manual
        | #[xref(target="SYSTEMD_TIME") systemd.time(7)].

      t
        | #[strong Note]: This solution assumes that
        | #[xref(target="XORG") Xorg] is used as display server instead of
        | #[xref(target="WAYLAND") Wayland].
        | To use the Xorg by default, add the following line in
        | #[em /etc/gdm/custom.conf] under the #[strong daemon] section:
      sourcecode(type='bash')
        | [daemon]
        | ...
        | WaylandEnable=false

      t Finally, start and enable the two timers:
      sourcecode(type='bash')
        | systemctl enable --now dpms-on.timer dpms-off.timer

      t
        | #[strong Note]: Make sure the power-saving mode of the monitor is
        | disabled.

    section(anchor="SCHEDULED_UPDATES")
      name Scheduled updates

      t
        | It is #[bcp14 RECOMMENDED] to set up scheduled updates and reboot the
        | system just after them rather than letting them run automatically
        | since the latter may lead to system instability.
      t
        | For instance, the browser may crash if updates are made in background,
        | requiring its restart:
      figure(anchor="FIG_FIREFOX_CRASH")
        name Example of unstable system after automatic updates
        artwork(align='center' type='binary-art' src='fixed-display-guide/FirefoxCrash.png')

      t
        | The first thing to do is to disable any automatic updates by
        | disabling the relevant #[em systemd timers] if any:
      sourcecode(type='bash')
        | # List all active timers
        | systemctl list-timers
        | # and disable the proper one
        | systemctl disable --now dnf-automatic-install.timer

      t
        | Next, programmatically check for available updates and install them
        | if there are any using #[strong pkcon] (the command line client of
        | the cross-platform package system manager
        | #[xref(target="PACKAGEKIT") PackageKit]).
        | To achieve this, simply create a #[em systemd service] and the related
        | #[em systemd timer] in #[em /etc/systemd/system/] like the following:

      figure
        name system-scheduled-updates.service
        sourcecode(type="bash")
          | [Unit]
          | Description=System scheduled updates
          |
          | [Service]
          | Type=oneshot
          | # Refresh updates info and check for available ones
          | ExecStartPre=pkcon refresh force ; pkcon get-updates
          | # Update the system
          | ExecStart=pkcon update -y --autoremove
          | # Rebbot the system
          | ExecStartPost=systemctl reboot -i
          | User=&lt;user&gt;
          | Group=&lt;user&gt;

      figure
        name system-scheduled-updates.timer
        sourcecode(type="bash")
          | [Unit]
          | Description=System scheduled updates timer
          |
          | [Timer]
          | Unit=system-scheduled-updates.service
          | OnCalendar=&lt;time&gt;
          | AccuracySec=30s
          | Persistent=false
          |
          | [Install]
          | WantedBy=timers.target

      t
        | By setting the #[strong &lt;time&gt;] properly, the system
        | periodically checks for new updates, installs them when
        | available and finally reboots. In case there are none, the
        | system will not be rebooted.
      t
        | A good choice #[bcp14 MAY] be a weekly schedule, like
        | #[em Wed, 20:30:00]. For more information about time see the
        | manual #[xref(target="SYSTEMD_TIME") systemd.time(7)].
      t
        | #[strong Note]: The &lt;user&gt; and &lt;group&gt; #[bcp14 MUST] be
        | those of the system administrator, for instance #[em root] and
        | #[em root].

      t
        | #[strong Note]: The  #[em --autoremove] pkcon option may be not
        | available on some system. Check it with #[em pkcon --help]

      t Enable the timer as final step:
      sourcecode(type='bash')
        | systemctl enable --now system-scheduled-updates.timer

    section(anchor="REMOTE_DEBUGGING")
      name Remote debugging
      t
        | There may be cases where it is required to do debug a web application
        | and complicated to access the machine physically. In such cases,
        | enabling the remote debugging can help.

      t
        | The idea is to connect the Firefox Web DevTools front-end to the
        | remotely running Firefox instance by using the
        | #[xref(target="REMOTE_DEBUGGING_PROTOCOL") Remote Debugging Protocol].

      t
        | To do that, you need to change some configuration preferences of the
        | remote Firefox browser first. It can be done programmatically by
        | specifying them in the #[strong user.js] file, located inside the
        | above-mentioned profile folder (#[em ~/.mozilla/firefox/app1]).
      t
        | According to the script #[em openFixedDisplay.sh] above, the
        | #[em user.js] file #[bcp14 SHOULD] appear as follow:
      sourcecode(type='bash')
        | user_pref("devtools.debugger.remote-enabled", true);
        | user_pref("devtools.chrome.enabled", true);
        | user_pref("devtools.debugger.prompt-connection", false);

      t
        | In addition, it is necessary to start the debug server together
        | with the Firefox instance on a given port through the option:
      sourcecode(type='bash')
        | --start-debugger-server 6000

      t
        | Once the web application is in operation, it is important to create a
        | tunnel between the local host (the debug-client used for debugging)
        | and the remote host on which the app to be debugged is running
        | (debug-server):
      sourcecode(type='bash')
        | # local port forwarding
        | ssh -4 -L 6000:localhost:6000 &lt;debug-server&gt; -N

      t
        | At this point, launch Firefox on the debug-client machine, navigate
        | to the #[em about:debugging] page and add the #[em localhost:6000]
        | host through the network location setting.
      figure
        name Network location
        artwork(align='center' type='binary-art' src='fixed-display-guide/network_location.png')

      t
        | Connect to the debug-server using the dedicated #[em Connect] button.
      figure
        name Remote browser instance connection
        artwork(align='center' type='binary-art' src='fixed-display-guide/remote_browser_connection.png')

      t
        | Clicking then on the connected device (1), the page will display a
        | Tab section with all the tabs opened on the remote server.
        | Finally, you can debug the web app by clicking on the corresponding
        | #[em Inspect] button (2).
      figure
          name Remote browser instance tabs
          artwork(align='center' type='binary-art' src='fixed-display-guide/remote_browser_tabs.png')

      t
        | #[strong Note]: Although this guide has been written with reference to
        | the Firefox browser, it is possible to remotely debug using other browsers
        | like Chrome/Chromium browser. To find out more see the
        | #[xref(target="CHROME_DEVTOOLS_PROTOCOL") Chrome DevTools Protocol]
  back
    references
      name References

      references
        name Normative References
        | &rfc2119;

      references
        name CERN Sites and Repositories

        reference(anchor='LINUX_INSTALLATION' target='https://linux.web.cern.ch/installation/pxeboot/')
          front
            title How to install Linux at CERN (using PXE network boot)
            author
              organization CERN

        reference(anchor='SERVICE_ACCOUNT' target='https://account.cern.ch/account/Management/NewAccount.aspx')
          front
            title Service account
            author
              organization CERN

        reference(anchor='AUTH_GET_SSO_COOKIE' target='https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie')
          front
            title auth-get-sso-cookie
            author
              organization CERN

        reference(anchor='VUE_SITE_SHOW_SCRIPTS' target='https://gitlab.cern.ch/mro/common/www/vue-site-show/-/tree/master/deploy')
          front
            title Vue-Site-Show scripts
            author
              organization CERN

      references
        name Others

        reference(anchor='SYSTEMD_TIME' target='https://man7.org/linux/man-pages/man7/systemd.time.7.html')
          front
            title systemd.time - Time and date specifications
            author(fullname='Michael Kerrisk')

        reference(anchor='XORG' target='https://www.x.org/wiki/')
          front
            title Xorg
            author
              organization X.Org Foundation

        reference(anchor='WAYLAND' target='https://wayland.freedesktop.org/')
          front
            title Wayland
            author(fullname="Kristian Høgsberg")

        reference(anchor='REMOTE_DEBUGGING_PROTOCOL' target='https://firefox-source-docs.mozilla.org/devtools/backend/protocol.html#remote-debugging-protocol')
          front
            title Remote Debugging Protocol
            author
              organization Mozzilla Foundation

        reference(anchor='CHROME_DEVTOOLS_PROTOCOL' target='https://chromedevtools.github.io/devtools-protocol/')
          front
            title Chrome DevTools Protocol
            author
              organization The Chrome team

        reference(anchor='PACKAGEKIT' target='https://www.freedesktop.org/software/PackageKit/index.html')
          front
            title PackageKit
            author(fullname="Richard Hughes")
