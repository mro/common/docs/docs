using namespace smc;

// method prototype:
std::future<void> Axis::moveTo(const units::Value &position) const;

// move an axis and wait for movement end
// (considering motionController has already been created):
Axis::Shared axis = mc.getDevice<Axis>("grbl://1/axis/X");
axis->moveTo(75.0 * units::mm).wait();
