POST /dis/command HTTP/1.1
Accept: application/json, text/plain, */*
Content-Type: application/json;charset=UTF-8

{
  "definition": "C,C|RPC",
  "sid": 1,
  "name": "DIS_DNS/SERVICE_INFO/RpcIn",
  "data": "*/ListDaqElements"
}
