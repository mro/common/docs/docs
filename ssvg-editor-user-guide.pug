<?xml version='1.0' encoding='utf-8'?>
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
//- <!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY ssvg-specification-03 SYSTEM "references/reference.ssvg-specification-03.xml">

// local reference to another doc
| ]>

// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>


// categories: std | bcp | info | exp | historic
rfc(category='bcp' number='2820020' consensus='false' ipr='trust200902' docName='ssvg-editor-user-guide-01' submissionType='be-cem-mro' xml:lang='en' version='3')
  front
    title(abbrev='SSVG Editor User Guide') SSVG Editor User Guide
    author(fullname='Sylvain Fargier' initials='S.F.' role='editor' surname='Fargier')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email sylvain.fargier@cern.ch
    date(year='2023' month="January")

    keyword SSVG
    keyword user-guide

    abstract
      t This document is a user-guide for the SSVG Editor Application.

  middle

    section(anchor="SEC_INTRODUCTION")
      name Introduction
      t
        | This is a user-guide for the SSVG Editor Application.
        | It describes the different features provided by the application as well
        | as some best-current practices.

      t The SSVG Editor application is available here: #[eref(target='https://mro-dev.web.cern.ch/ssvg-editor')].

      t The beta version is available #[eref(target='https://mro-dev.web.cern.ch/beta/ssvg-editor') here], the source-code is #[xref(target='SSVG_EDITOR') there].

      // This one is a 'must' :)
      //- section
      //-   name Requirements Notation
      //-   t
      //-     | The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
      //-     | "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
      //-     | document are to be interpreted as described in BCP 14,
      //-     | #[xref(target='RFC2119' format='default') RFC2119].

      section
        name What is SVG

        t A SVG (Scalable Vector Graphics) document is:
        ul
          li A #[xref(target='W3C') W3C] standard (#[xref(target="SVG") SVG-1.1])
          li
            | An #[xref(target='XML') XML-based] document that describes
            | two-dimensional graphics (source: #[eref(target="https://developer.mozilla.org/en-US/docs/Web/SVG") MDN]) composed of
            | #[eref(target="https://developer.mozilla.org/en-US/docs/Web/SVG/Element") graphical elements], ex: rect, circle ...
          li
            t A #[eref(target="https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model") DOM] compatible document that:
            ul
              li Integrates #[eref(target="https://developer.mozilla.org/en-US/docs/Web/JavaScript") Javascript]
              li Can be navigated using #[eref(target="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors") CSS Selectors], ex: id, class ...
              li Can be styled using #[eref(target="https://developer.mozilla.org/en-US/docs/Web/CSS") CSS]

      section(anchor="SEC_WHAT_IS_SSVG")
        name What is SSVG

        t SSVG (Stateful SVG) is:
        ul
          li An #[xref(target="ssvg-specification-03") extension to SVG documents] : 3 main elements, 7 in total
          li
            t A declarative #[strong state] for graphical document to describe:
            ul
              li logical state #[strong properties], ex: temperature, position ...
              li #[strong relations] between properties and graphical elements

        figure
          name SSVG state declaration
          sourcecode(type='XML' src='ssvg-editor-user-guide/gauge-state-example.xml')

        figure
          name SSVG state illustation
          artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/gauge-state-illustration.png')

      section
        name What is SSVG Editor

        t A SSVG Editor is:
        ul
          li A graphical tool to #[strong manipulate] and #[strong edit] SSVG state declarations.
          li A #[strong preview] tool to display graphical state for a given logical state of a SSVG document.
          li
            t Something like this:
            figure
              name SSVG Editor screenshot
              artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-screenshot.png')

        t A SSVG Editor is #[strong not]:
        ul
          li
            | A SVG editor, it can't be used to edit the graphical part of SSVG (or SVG),
            | there are better tools for this, ex: #[eref(target="https://inkscape.org") Inkscape],
            | #[eref(target="https://app.diagrams.net") draw.io], #[eref(target="https://boxy-svg.com") Boxy-SVG] ...
          li
            | An animation engine : there are dedicated #[xref(target="SSVG_ENGINE") ssvg-engine implentation(s)] that
            | can be embedded in your applications.

    section
      name Overview

      section
        name Application Layout

        t Here's the SSVG Editor general layout, each part will be presented in a dedicated section:
        figure
          name SSVG Editor layout
          artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-layout.png')

      section
        name Documentation

        t
          | The SSVG Editor embeds its own contextual documentation, that links
          | to external documentation for more advanced information and hints
          | about possible actions and features.

        figure
          name Contextual Documentation
          artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-contextual-doc.png')

    //- section
    //-   name Welcome Screen

    section(anchor='SEC_GLOBAL_CONTROLS')
      name Global Controls

      t The #[strong Global Controls] widget allows control over the loaded document.

      figure
        name Global Controls
        artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-global-controls.png')

      ul
        li #[strong Clear] : reset application to its default state, any un-saved modification is lost (there is a confirmation dialog)
        li #[strong Export] : Export document to clipboard or download as a file
        li #[strong Reset Zoom] : Reset preview zoom to its default value
        li #[strong Undo / Redo] : undo/redo last/previous document modification
        li #[strong Reload] : reload document and re-initialize preview engine
        li
          t #[strong ViewMode] : change view-mode, available view modes:
          ul
            li #[strong normal] : normal preview
            li #[strong jump] : all transitions are disabled
            li
              | #[strong shadow] (default) : two previews are superposed, a semi
              | transparent in #[tt jump] mode (that shows where the graphics
              | will move) and a solid #[tt normal] one

      t #[strong Note:] the #[tt ViewMode] is made persistent for a given browser (saved in browser's local-storage).

    section
      name Document Preview

      t
        | The #[strong Document Preview] widget displays the document as rendered by
        | the #[xref(target='SSVG_ENGINE') ssvg-engine].

      t
        | It will animate and reflect any state modifications, as described in
        | #[xref(target='SEC_STATE_MANIPULATION') State Manipulation].

      t
        | It also supports graphics navigation, using mouse-drag to move the image, and the mouse wheel to zoom in/out.

      figure
        name Document Preview Card
        artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-preview.png')

      t
        | #[strong Note:] The Document Preview will be automatically reloaded on properties, relation or transform modifications.

    section(anchor='SEC_TIMELINE')
      name Timeline

      t
        | The #[strong Timeline] widget graphically displays the value of the currently selected property.

      t
        | By using mouse-drag to move the arrows it also allows direct-control over the selected property.

      t
        | Whenever a relation, transform or direct element is selected, its
        | key-times will also be displayed (see #[xref(target='ssvg-specification-03')] SSVG specification);
        | by clicking on one of these, the property will be updated to this value.

      t
        | Hovering over those elements (key-time, or arrows) a tooltip revealing the associated #[tt key-time] and #[tt target-value] will be displayed.

      figure
        name Timeline Card
        artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-timeline.png')

    section
      name Document Navigation

      t
        | The left-pane is dedicated to the document content navigation.

      section(anchor='SEC_PROPERTIES_LIST')
        name Properties

        t
          | The #[strong Properties] card is a tree-view displaying the SSVG state's element content,
          | it is used to select the element that you want to modify.

        t It may also be used to modify the current document state, see #[xref(target='SEC_STATE_MANIPULATION') State Manipulation] for further details.

        t
          | It may also be used to create properties, relations or transitions (or other state child elements).

        figure
          name Properties Card
          artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-properties-card.png')

        t
          | #[strong Note:] there may be several state elements in a single SSVG document (since SVG documents may be nested),
          | all should be listed as separate cards.

      section(anchor='SEC_CSS_SELECTORS')
        name CSS Selectors

        t
          | The #[strong CSS Selectors] card is a list of CSS Selectors available in the document.

        t
          | Clicking on a selector will highlight associated elements in the preview,
          | the selector may also be set on the selected property or relation element (by clicking on "set").

        t
          | When creating a relation, the target elements may also
          | be chosen by clicking in this list.

        figure
          name CSS Selectors Card
          artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-css-selectors.png')

        t
          | #[strong Note:] The list is limited to 250 elements, either taken from #[tt class] and #[tt id] selectors
          | that are set in the document, or generic tag navigation selectors (the ones starting with #[tt "scope:"]).

        t
          | #[strong Note:] The #[tt class] and #[tt id] selectors should be prefered, but may be
          | available only on properly prepared SVG documents.

    section
      name Editors

      section
        name State Editor

        t
          | The #[strong State Editor] card displays the current document logical state in
          | an XML editor.

        t
          | It may be used to modify the current state by editing it and clicking on
          | "apply".

        figure
          name State Editor Card
          artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-state-editor.png')

      section(anchor='SEC_PROPERTY_EDITOR')
        name Property Editor

        t
          | The #[strong Property Editor] card displays the selected property
          | (see #[xref(target='SEC_PROPERTIES_LIST')]) in an XML editor.

        t
          | This editor is the main widget to edit and manipulate the different state
          | elements, it provides advanced features such as:
        ul
          li syntax and grammar validation
          li context-sensitive code-completion
          li in-editor widgets such as CSS selector and color selector.

        figure
          name Property Editor Card
          artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-property-editor.png')

        t #[strong Note:] It may also be used to delete an element and all its content by clicking on "delete".

        t #[strong Note:] Modifications achieved using this editor my be reverted using the "undo" #[xref(target="SEC_GLOBAL_CONTROLS") global-control] button.

      section
        name Relation and Transform Editors

        t
          | The #[strong Relation Editor] and #[strong Transform Editor] cards
          | displays the selected relation and transform providing the same feature-set than
          | the #[xref(target='SEC_PROPERTY_EDITOR') Property Editor].

        t
          | In addition to this, it also provides a #[tt Live Value] section
          | that shows the current target-value with regards to the current logical state.
          | This live-value may be modified to test the modification effects on the preview.

        figure
          name Transform Editor Card
          artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/ssvg-editor-transform-editor.png')

    section
      name Best Current Practices

      section(anchor='SEC_STATE_MANIPULATION')
        name State manipulation

        t To manipulate the current document state, the following widgets are available:
        figure
          name State manipulation widgets
          artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/bcp-state-manip/state-manipulation.png')

        t #[strong Note:] manipulating the SSVG state doesn't modify the SSVG document, it applies the state changes to the preview.

      section(anchor='SEC_COLOR_POSITION_ANIMATION')
        name Animating color and position

        t
          | This section describes how a simple SVG such as the gauge displayed in #[xref(target="SEC_WHAT_IS_SSVG") Introduction]
          | can be animated.

        section
          name SVG preparation

          t
            | Create your drawing using a SVG editor such as #[eref(target="https://inkscape.org") Inkscape],
            | the gauge is simply 3 paths elements.

          t
            | Prepare what you want to animate and display, first the color, you'd have to identify the inner path for this:
          ol
            li Select the inner path
            li Go to object-properties and set the #[tt ID] as "gauge" (shortcut:  #[tt 'ctrl-shift-O'])

          t
            | To animate the gauge we decided to use a clip-path (one of the SVG elements available):
          ol
            li Draw a rectangle that entirely overlaps the inner path
            li Disable the rectangle's stroke and set its fill, using "Fill and Stroke" (shortcut: #[tt ctrl-shift-F])
            li Go to object-properties and set the #[tt ID] as "clipRect" (shortcut:  #[tt 'ctrl-shift-O'])
            li Select both the rectangle and the inner path, using "Layers and Objects" (shortcut: #[tt ctrl-shift-L])
            li Set the rectangle as clip (Object-&gt;Clip&gt;Set Clip)

          t
            | Now if you change the #[tt y] value of the rectangle you can see your gauge level being clipped,
            | producing the desired effect, this is a basic SVG technique that you can find in
            | #[eref(target='https://inkscape-manuals.readthedocs.io/en/latest/clipping-and-masking.html') Inkscape manual].

        section
          name SSVG creation

          t Using the SSVG editor:
          ol
            li Drop the image in SSVG Editor
            li
              t Create a property as:
              figure
                name Add Property Dialog
                artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/bcp-color-pos/create_property_dialog.png')
            li
              t Select the property and create a #[tt relation] (+)
              t Click on the inner path or on its ID selector in the #[xref(target='SEC_CSS_SELECTORS') CSS Selectors list].
              t Configure it as:
              figure
                name Color Relation Creation
                artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/bcp-color-pos/create_color_relation_dialog.png')
            li
              t Select the property and create another #[tt relation] (+)
              t Select the #[tt clipPath] ID selector in the #[xref(target='SEC_CSS_SELECTORS') CSS Selectors list].
              t Configure it as:
              figure
                name Position Relation Creation
                artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/bcp-color-pos/create_position_relation_dialog.png')
              t Use the "Live Values" to adjust the #[tt clipPath] position.

          t The generated document should look like:
          figure
            name Example SSVG gauge
            sourcecode(type='XML' src='ssvg-editor-user-guide/bcp-color-pos/gauge-simple.svg')

      section(anchor='SEC_TEXT_ANIMATION')
        name Animating a text

        t This section is a short guide to animate a text on a plain SVG document.

        section
          name SVG preparation

          t Using a SVG editor such as #[eref(target="https://inkscape.org") Inkscape]:
          ol
            li Create a rectangle and a text (shortcuts: #[tt 'R'] then #[tt 'T'])
            li Go to the XML editor (shortcut: #[tt 'ctrl-shift-X'])
            li
              t Select your text and add the following attributes:
              ul
                li #[tt 'dominant-baseline="middle"']
                li #[tt 'text-anchor="middle"'] (valid values are: start, middle and end)
                li #[tt 'id="my_text_identifier"'] (can also be achieved using #[tt 'ctrl-shift-O'])
            li Align the text in the rectangle
            li Save the document as "Optimized SVG" (shortcut: #[tt 'ctrl-shift-S']), don't forget to untick "Remove unused IDs"

        section
          name SSVG creation

          t Using the SSVG editor:
          ol
            li Drop the image in SSVG Editor
            li
              t Create a property, typed either as "number" or "auto" (naming it #[tt 'Property#field'] for CMW/WRAP integration).
              figure
                name Create Property Button
                artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/bcp-text-anim/create_property.png')
              figure
                name Add Property Dialog
                artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/bcp-text-anim/create_property_dialog.png')
            li
              t Select the property and create a #[tt direct] relation (+)
              figure
                name Create Property Button
                artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/bcp-text-anim/create_direct_relation.png')
              t Click on the text (on the graphics) or on its ID selector in the #[xref(target='SEC_CSS_SELECTORS') CSS Selectors list].
            li
              t Leave everything blank unless #[tt onupdate] (see the inline help)
              figure
                name Add Property Dialog
                artwork(align='center' type='binary-art' src='ssvg-editor-user-guide/bcp-text-anim/create_direct_relation_dialog.png')

          t The generated document should look like:
          figure
            name Example text relation
            sourcecode(type='XML' src='ssvg-editor-user-guide/example-text-relation.xml')

  back

    references
      name References
      references
        name Normative References
        //- | &rfc2119;
        | &ssvg-specification-03;

      //- references
      //-   name Informative References

      references
        name URL References

        reference(anchor='W3C' target='https://www.w3.org/')
          front
            title The World Wide Web Consortium (W3C)
            author
              organization W3C, Inc.
            date(year='1994')

        reference(anchor='XML' target='www.w3.org/XML/')
          front
            title Extensible Markup Language (XML)
            author
              organization W3C, Inc.
            date(year='1996')

        reference(anchor='SVG' target='www.w3.org/TR/SVG11/')
          front
            title Scalable Vector Graphics (SVG) 1.1 (Second Edition)
            author
              organization W3C, Inc.
            date(year='2011')

        reference(anchor='SSVG_ENGINE' target='https://gitlab.cern.ch/mro/common/www/ssvg-engine')
          front
            title SSVG implementation in Javascript
            author
              organization CERN
            date(year="2021")

        reference(anchor='SSVG_EDITOR' target='https://gitlab.cern.ch/mro/common/www/ssvg-editor')
          front
            title SSVG Editor Application
            author
              organization CERN
            date(year="2022")

      //-   reference(anchor='xml2rfc' target='http://xml.resource.org')
      //-     front
      //-       title XML2RFC tools and documentation
      //-       author
      //-         organization RFC Editor
      //-       date(year='2013')

    //- section
    //-   name Change Log
    //-   t A list of changes

    //- section
    //-   name Open Issues
    //-   t A list of open issues regarding this document
