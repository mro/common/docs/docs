# EN-SMM-APC documentation stream

Here are source documents for the BE-CEM-MRO stream.

Documents are published on [MRO docs](http://mro-dev.web.cern.ch/docs/).

## Documents generation

To generate documentation it is recommended to use the docker container as described in [xml2rfc README](https://gitlab.cern.ch/mro/common/docs/xml2rfc/blob/master/README.md):
