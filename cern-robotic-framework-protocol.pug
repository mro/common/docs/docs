<?xml version='1.0' encoding='utf-8'?>
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
<!ENTITY rfc793 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.0793.xml">
<!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY rfc8259 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.8259.xml">
| ]>

// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>

mixin CRF
  xref(target='CRF') CRF

// categories: std | bcp | info | exp | historic
rfc(category='std' number='2820019' consensus='false' ipr='trust200902' docName='cern-robotic-framework-protocol-01' submissionType='be-cem-mro' xml:lang='en' version='3')
  front
    title(abbrev='CRF protocol') CERN Robotic Framework Protocol
    author(fullname='Sylvain Fargier' initials='S.F.' role='editor' surname='Fargier')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email sylvain.fargier@cern.ch
    date(year='2022' month="September" day="13")

    keyword CRF
    keyword protocol
    keyword TCP
    keyword JSon

    abstract
      t
        | This document describes the CRF (CERN Robotic Framework) protocol, a
        | socket based protocol (or stack of protocols) developped by BE-CEM-MRO team.

      t
        | The CRF is a multi-applicative layered protocol, it lies on OSI layer 7 but implements
        | its own transport layer to proxy third-party payloads.

  middle
    section(anchor="CRF")
      name Introduction
      t
        | This document describes the CRF (CERN Robotic Framework) protocol, as
        | developed and used by the BE-CEM-MRO team to control and interact with
        | robots.

      t
        | The #[+CRF] is a multi-applicative layered protocol, it lies on #[xref(target='OSI_MODEL') OSI layer 7]
        | but implements its own transport layer to proxy third-party protocols payloads.

      t
        | On the top of its transport layer the #[+CRF] implements several dediacted protocols, to #[xref(target="Stream_protocol") Stream Data]
        | or #[xref(target="Control_protocol") control devices].

      // This one is a 'must' :)
      section
        name Requirements Notation
        t
          | The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
          | "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
          | document are to be interpreted as described in BCP 14,
          | #[xref(target='RFC2119' format='default') RFC2119].

    section(anchor="Overview")
      name General Overview

      t
        | This protocol is split in two parts, a transport and packet-framing header as described in #[xref(target='Data_Packet')] and
        | an applicative payload, that can be either part of the CRF, such as the #[xref(target='Control_protocol') JSon Commands Protocol]
        | and the #[xref(target='Stream_protocol') Data Streaming Protocol], or a third-party protocol frame, such as the #[tt Kuka arm status frame].

      t
        | Third-party protocol frames won't be covered in this document.

    section(anchor="Implementations")
      name Implementations

      t
        | The #[+CRF] is implemented in several languages, such as #[xref(target='CPP_CRF') C++]
        | and #[xref(target='PY_CRF') Python], using either
        | IPv4 or UNIX #[xref(target='RFC0793') TCP] stream sockets as network and transport layers.

    section(anchor="Data_Packet")
      name Data Packet

      t
        | The CRF data packet is an encapsulates the applicative payload,
        | it provides payload type identification.

      figure
        name Data Packet Header
        //- generated using https://github.com/luismartingarcia/protocol.git
        //- ```protocol 'Sync (0x7E '~')x6:48,Type:16,Length:32,Timestamp:64,WriterID:8,CRC:8,Payload... (length sized):32' -b 32 --oddchar - --evenchar -```
        artwork(align='center' type='ascii-art' name='' alt='').
          0                   1                   2                   3
          0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
          +---------------------------------------------------------------+
          |                       Sync (0x7E '~')x6                       |
          +                               +-------------------------------+
          |                               |              Type             |
          +---------------------------------------------------------------+
          |                             Length                            |
          +---------------------------------------------------------------+
          |                                                               |
          +                           Timestamp                           +
          |                                                               |
          +---------------------------------------------------------------+
          |   WriterID    |      CRC      |   Payload... (length sized)   |
          +---------------------------------------------------------------+

      t All fields #[bcp14 MUST] be interpreted as little-endian encoded unsigned integers.

      ul
        li #[strong Sync] field is a synchronization pattern (6 times '~' character)
        li #[strong Type] field defines the payload type as described in #[xref(target='Data_Packet_Types') Data Packet Types].
        li #[strong Length] field is the payload length in bytes (header-size excluded).
        li
          | #[strong Timestamp] field contains the message creation timestamp, in nano-seconds from Epoch
          | (as described in #[xref(target="POSIX_base") POSIX Base Specifications]).
        li #[strong Writer ID] is used as a #[xref(target='Writer_Identifier') message writer identifier].

      t #[tt CRC] is not a real CRC, it is computed using the following formula:
      figure
        name CRC algorithm
        sourcecode.
          CRC = (Type + Length + Timestamp + WriterID) % 255

      t #[strong Note:] current implementation doesn't enforce endianness.
      t #[strong Note:] Timestamp and WriterID are not used in current implementation.

      section(anchor='Writer_Identifier')
        name Writer Identifier

        t
          | The #[tt WriterID] field #[bcp14 MAY] be used to identify a specific writer and/or a
          | message sequence.

        t
          | The #[tt WriterID] field of replies packets sent by a given #[+CRF] service provider #[bcp14 MUST]
          | match the #[tt WriteID] of the client packet that the reply is associated to.

        t
          | This #[bcp14 MUST] also be effective with streaming commands, tagging the entire stream
          | with the same #[tt WriterID] that started the stream (even if #[tt start] command is
          | received using a #[xref(target="JSon_Command") JSon protocol] and packets are sent using #[xref(target="Stream_protocol") Streaming protocol].

      section(anchor="Data_Packet_Types")
        name Data Packet Types

        table(align='center')
          thead
            tr
              th(align='center') Name
              th(align='center') Type
              th(align='center') Description
          tbody
            tr #[td ARM_JOINT_BY_JOINT_POSITION] #[td 1] #[td unused]
            tr #[td ARM_WORLD_COORDINATE] #[td 2] #[td unused]
            tr #[td MOVE_BASE] #[td 3] #[td unused]
            tr #[td ARM_GRIPPER] #[td 4] #[td unused]
            tr #[td MOVE_BASE_PERCENTAGE] #[td 5] #[td unused]
            tr #[td ARM_JOINT_BY_JOINT_VELOCITY]#[td 6] #[td unused]
            tr #[td ARM_JOINT_BY_JOINT_POSITION_VEL] #[td 7] #[td unused]
            tr #[td ROBOT_F_T_BIAS_UPDATE] #[td 19] #[td unused]
            tr #[td ROBOT_ARM_STATUS_PACKET] #[td 21] #[td unused]
            tr #[td ROBOT_BASE_STATUS_PACKET] #[td 22] #[td unused]
            tr #[td ROBOT_BASE_LIFTING_STAGE_STATUS_PACKET] #[td 23] #[td unused]
            tr #[td SCHUNK_ARM_STATUS_PACKET] #[td 24] #[td unused]
            tr #[td ROBOT_MICROCONTROLLER_STATUS_PACKET] #[td 26] #[td unused]
            tr #[td MOTORS_STATUS_PACKET] #[td 28] #[td unused]
            tr #[td URROBOT_STATUS_PACKET] #[td 29] #[td unused]
            tr #[td URROBOT_DASHBOARD_PACKET] #[td 30] #[td unused]
            tr #[td RP_SENSOR_STATUS_PACKET] #[td 35] #[td unused]
            tr #[td RP_SENSOR_DISTANCE_STATUS_PACKET] #[td 36] #[td unused]
            tr #[td SICK300_LASER_PACKET] #[td 37] #[td unused]
            tr #[td SCREWDRIVER_CURRENT_PACKET] #[td 41] #[td unused]
            tr #[td SCREWDRIVER_PULSE_PACKET] #[td 42] #[td unused]
            tr #[td SCREWDRIVER_STATUS_PACKET] #[td 43] #[td unused]
            tr #[td OIL_PUMP_PACKET] #[td 44] #[td unused]
            tr #[td ELECTROVALVE_PACKET] #[td 47] #[td unused]
            tr #[td REAL_SENSE_POINT_PACKET] #[td 48] #[td unused]
            tr #[td ORBBEC_PRO_POINT_PACKET] #[td 49] #[td unused]
            tr #[td WEIGHT_SCALE_WEIGHT] #[td 89] #[td unused]
            tr #[td POINT_3D_PACKET] #[td 90] #[td unused]
            tr #[td POINT_2D_PACKET] #[td 91] #[td unused]
            tr #[td KUKA_ARM_STATUS_PACKET] #[td 92] #[td unused]
            tr #[td COLLISION_STATUS_PACKET] #[td 93] #[td unused]
            tr #[td SLAM_TRACKER_PACKET] #[td 94] #[td unused]
            tr #[td THERMAL_CAMERA_PACKET] #[td 95] #[td unused]
            tr #[td XLS_ADAPTER_PACKET] #[td 96] #[td unused]
            tr #[td ARM_POSE_WORLD_COORDINATE] #[td 104] #[td unused]
            tr #[td ARM_MATRIX_WORLD_COORDINATE] #[td 105] #[td unused]
            tr #[td ARM_WORLD_COORDINATE_RELATIVE] #[td 106] #[td unused]
            tr #[td Depth_Point_Packet] #[td 107] #[td unused]
            tr #[td RGB_Depth_Point_Packet] #[td 108] #[td unused]
            tr #[td JPEG_IMAGE_PACKET] #[td 109] #[td unused]
            tr #[td ARM_WORLD_COORDINATE_COLLISION] #[td 110] #[td unused]
            tr #[td ROBOT_F_T_DATA] #[td 111] #[td unused]
            tr #[td CAMERA_SETTING_PACKET] #[td 112] #[td unused]
            tr #[td ROBOT_ALIGNMENT] #[td 113] #[td unused]
            tr #[td ROBOT_WORLD_ALIGNMENT] #[td 114] #[td unused]
            tr #[td REALSENSE_STREAM_PACKET] #[td 115] #[td unused]
            tr #[td RGBD_CAMERA_SETTING_PACKET] #[td 116] #[td unused]
            tr #[td UWB1D_DISTANCE_PACKET] #[td 117] #[td unused]
            tr #[td RAIL_MOTOR_PACKET] #[td 118] #[td unused]
            tr #[td FRAME_PACKET] #[td 119] #[td #[xref(target='Framing_Protocol') CRF Framing protocol]]
            tr #[td CONTROLLINO_STATUS_PACKET] #[td 120] #[td unused]
            tr #[td CONTROLLINO_CONFIGURE_DIGITAL_PIN] #[td 121] #[td unused]
            tr #[td CONTROLLINO_SET_PIN_VALUE] #[td 122] #[td unused]
            tr #[td RGBD_FRAME_PACKET] #[td 123] #[td #[xref(target='RGBD_Framing_Protocol') CRF RGBD Framing protocol]]
            tr #[td LINEAR_STAGE_POSITION_PACKET] #[td 201] #[td unused]
            tr #[td LINEAR_STAGE_VELOCITY_PACKET] #[td 202] #[td unused]
            tr #[td LINEAR_STAGE_STATUS_PACKET] #[td 203] #[td unused]
            tr #[td GSM_INFO_STATU_PACKET] #[td 204] #[td unused]
            tr #[td SYSTEM_INFO_PACKET] #[td 301] #[td unused]
            tr #[td VELODYNE_POSE] #[td 401] #[td unused]
            tr #[td User_RGBD] #[td 503] #[td unused]
            tr #[td AREA_STILL_NO_CLICKED] #[td 601] #[td unused]
            tr #[td AREA_CLICKED_COORDINATE] #[td 602] #[td unused]
            tr #[td DIMENSION_OF_INTEREST_AREA] #[td 603] #[td unused]
            tr #[td IMU_DATA_PACKET] #[td 701] #[td unused]
            tr #[td JSON_PACKET] #[td 801] #[td #[xref(target='Control_protocol') CRF Control protocol]]
            tr #[td STREAM_PACKET] #[td 802] #[td unused]
            tr #[td LASER_PACKET] #[td 901] #[td unused]
            tr #[td UDP_CONNECTION_PACKET] #[td 1001] #[td unused]
            tr #[td FETCHWRITE_PACKET] #[td 1002] #[td unused]
            tr #[td TIME_SYNC] #[td 7001] #[td unused]
            tr #[td FIFO_CLOSE_PACKET] #[td 7002] #[td unused]

    section(anchor="Control_protocol")
      name CRF Control Protocol

      t
        | The #[+CRF] provides an #[eref(target='https://en.wikipedia.org/wiki/Remote_procedure_call') RPC] oriented
        | control protocol, it uses #[xref(target='RFC8259' format='default') JSon data format] to encode its messages.

      t
        | The control protocol is identified in #[xref(target="Data_Packet") Data Packets] as type #[tt 801].

      t
        | Messages sent using this protocol #[bcp14 MUST] be valid #[xref(target='RFC8259' format='default') JSon] objects, that matches
        | the schemas described below.

      section(anchor="JSon_Command")
        name JSon Command

        t(keepWithNext="true")
          | Here is a #[xref(target='JSON_SCHEMA') JSon-schema]
          | describing the CRF command packet syntax:
        figure
          name CRF Command Message Schema
          sourcecode(type="rnc" src="cern-robotic-framework-protocol/command.schema.json")

        t
          | The #[tt id] property is #[bcp14 OPTIONAL] in command packets, it #[bcp14 MUST] be
          | sent back un-modified by the service in associated #[xref(target="JSon_Reply") JSon replies]
          | or #[xref(target="JSon_Error") error messages].

      section(anchor="JSon_Reply")
        name JSon Reply

        t(keepWithNext="true")
          | Here is a #[xref(target='JSON_SCHEMA') JSon-schema]
          | describing the CRF reply packet syntax:
        figure
          name CRF Reply Message Schema
          sourcecode(type="rnc" src="cern-robotic-framework-protocol/reply.schema.json")

      section(anchor="JSon_Error")
        name JSon Error

        t(keepWithNext="true")
          | Here is a #[xref(target='JSON_SCHEMA') JSon-schema]
          | describing the CRF error packet syntax:
        figure
          name CRF Error Message Schema
          sourcecode(type="rnc" src="cern-robotic-framework-protocol/error.schema.json")

      section(anchor="Builtin_Commands")
        name Built-In Commands

        t
          | For the #[+CRF] protocol to be introspectable and discoverable, each #[xref(target="Control_protocol") Control protocol] endpoint #[bcp14 MUST] implement a set of built-in commands.

        section(anchor="Introspection")
          name Introspection

          t
            | The #[+CRF] protocol being really close to #[xref(target="JSON_RPC") JSon-RPC protocol], its introspection mechanism is based on #[xref(target="OPENRPC") OpenRPC] schemas.

          t
            | Each #[+CRF] endpoint #[bcp14 MUST] implement an #[tt introspect] method that #[bcp14 MUST] return a valid #[xref(target="OPENRPC") OpenRPC] document.

          t Here's an example introspection reply:
          figure
            name Service introspection reply example
            sourcecode(type="JSon" src="cern-robotic-framework-protocol/service-introspection-reply.json")

          t
            | To validate an introspection message, one can use #[tt jsonschema] Python library (amongst other choices)
            |on an #[xref(target="Introspect_Schema") introspection schema], see #[xref(target="Schema_Validation") examples] in annex.

          section
            name Service Identification

            t
              | The #[tt servers] object embedded at the root of the #[xref(target="OPENRPC") OpenRPC] document #[bcp14 SHOULD]
              | have at least one entry, its #[tt name] being used to identify a specific service (see example above).

          section
            name Methods Classification

            t The following #[xref(target="OPENRPC") OpenRPC tags ] #[bcp14 SHOULD] be used to better categorize #[tt methods]:
            ul
              li #[strong getter] when a method is a getter (no service modification).
              li #[strong setter] when a method is a setter (idempotent).

            t
              | Those two tags are mutually exclusive, methods not having any of those two tags are considered as non-idempotent
              | and may modify the service configuration/settings.

            figure
              name Method tag example
              sourcecode(type="JSon" src="cern-robotic-framework-protocol/method-tag-examples.json")

            t
              | Some #[+CRF] methods are used to control data or video #[xref(target="Stream_protocol") streams],
              | those methods #[bcp14 MUST] be linked together using a #[tt stop] #[xref(target="OPENRPC") OpenRPC] link
              | (shortened version, for complete version check the #[xref(target="Streaming_Introspection_Reply") streaming schema example]):
            figure
              name Streaming method example
              sourcecode(type="JSon" src="cern-robotic-framework-protocol/service-streaming-short.json")

        section(anchor="Discovery")
          name Discovery

          t
            | Each #[+CRF] endpoint #[bcp14 MUST] implement a simple discovery protocol, listening on #[tt UDP] port #[tt 11000] and serving the following #[xref(target="JSon_Command") command]:
          figure
            name Discovery method description
            sourcecode(type="JSon" src="cern-robotic-framework-protocol/discovery-method.json")

          t
            | #[+CRF] services #[bcp14 SHOULD] serve only #[tt discover] method on this service port, several services #[bcp14 MAY] be aggregated on a given server (thus the array in result).

          t
            | The #[tt discover] method #[bcp14 SHOULD] also be served on the service itself (#[tt TCP] or other listening socket).

    section(anchor="Stream_protocol")
      name Data Streaming Protocols

      t
        | The #[+CRF] supports several data streaming protocols, a #[xref(target="Framing_Protocol") Framing protocol] that is designed
        | to transport video frames, and an #[xref(target="RGBD_Framing_Protocol") RGBD Framing protocol] that transports depth and/or point-cloud augmented
        | video frames.

      section(anchor="Framing_Protocol")
        name Framing Protocol

        t
          | The framing protocol is identified in #[xref(target="Data_Packet") Data Packets] as type #[tt 119].

        t
          | it is generally coupled with a #[xref(target='Control_protocol') Control Protocol] service that
          | configures and starts the stream, once started, JPEG, X264 or CV_Mat frames are sent at the requested frame-rate.

        t(keepWithNext="true")
          | Each frame starts with a #[tt Type] byte followed by its associated payload:
        figure
          name Framing Protocol packet
          //- ```protocol 'Type:8,Payload...:24' -b 32 --oddchar - --evenchar -```
          artwork(align='center' type='ascii-art' name='' alt='').
             0                   1                   2                   3
             0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
            +---------------------------------------------------------------+
            | Encoding Type |                   Payload...                  |
            +---------------------------------------------------------------+

        t The following encoding types are supported:
        table(align='center')
          thead
            tr
              th(align='center') Name
              th(align='center') Value
              th(align='center') Description
          tbody
            tr #[td JPEG] #[td 1] #[td #[xref(target="JPEG_Frame") JPEG Frames]]
            tr #[td CV_MAT] #[td 2] #[td #[xref(target="CV_MAT_Frame") OpenCV Matrices]]
            tr #[td X264] #[td 3] #[td #[xref(target="X264_Frame") X264 Frames]]

        section(anchor="JPEG_Frame")
          name JPEG Frames

          t(keepWithNext="true")
            | The framing protocol #[bcp14 MAY] transport #[eref(target='https://www.w3.org/Graphics/JPEG') JPEG] images encoded as follows:
          figure
            name JPEG frames
            //- generated using https://github.com/luismartingarcia/protocol.git
            //- ```protocol '00:8,00:8,00:8,01:8,Image Size (in bytes):32,JPEG Image Data... (Image Size x bytes):32' -b 32 --oddchar - --evenchar -```
            artwork(align='center' type='ascii-art' name='' alt='').
               0                   1                   2                   3
               0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
              +---------------------------------------------------------------+
              |       00      |       00      |       00      |       01      |
              +---------------------------------------------------------------+
              |                     Image Size (in bytes)                     |
              +---------------------------------------------------------------+
              |            JPEG Image Data... (Image Size x bytes)            |
              +---------------------------------------------------------------+

          t
            | #[strong Note:] Each #[xref(target="Framing_Protocol") frame] #[bcp14 MAY] embed several JPEG frames (the #[tt Type=0x01] byte not being repeated),
            | but current implementation #[bcp14 SHOULD] send only a single JPEG frame per packet.

        section(anchor="CV_MAT_Frame")
          name OpenCV Matrices Frames

          t(keepWithNext="true")
            | The framing protocol #[bcp14 MAY] transport #[eref(target='https://docs.opencv.org') OpenCV] Matrices (see #[tt cv::Mat]) encoded as follows:
          figure
            name OpenCV matrices frames
            //- generated using https://github.com/luismartingarcia/protocol.git
            //- ```protocol '00:8,00:8,00:8,01:8,Image Size (in bytes):32,JPEG Image Data... (Image Size x bytes):32' -b 32 --oddchar - --evenchar -```
            artwork(align='center' type='ascii-art' name='' alt='').
               0                   1                   2                   3
               0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
              +---------------------------------------------------------------+
              |       00      |       00      |       00      |       01      |
              +---------------------------------------------------------------+
              |                          Matrix Type                          |
              +---------------------------------------------------------------+
              |                           Rows (i32)                          |
              +---------------------------------------------------------------+
              |                         Columns (i32)                         |
              +---------------------------------------------------------------+
              |                      Elements Size (u32)                      |
              +---------------------------------------------------------------+
              |            Data (Elements Size x Rows x Cols bytes)           |
              +---------------------------------------------------------------+

          t
            | #[strong Note:] Each #[xref(target="Framing_Protocol") Frame] #[bcp14 MAY] embed several OpenCV matrices frames (the #[tt Type=0x02] byte not being repeated),
            | but current implementation #[bcp14 SHOULD] send only a single OpenCV matrices frame per packet.

          t
            | #[strong Note:] for more information about #[tt Matrix Type] values, see #[tt cv::Mat::type] in #[eref(target='https://docs.opencv.org') OpenCV] documentation.

        section(anchor="X264_Frame")
          name X264 Frames

          t(keepWithNext="true")
            | The framing protocol #[bcp14 MAY] transport #[eref(target='https://en.wikipedia.org/wiki/X264') X264] stream encoded as follows:
          figure
            name X264 stream
            //- generated using https://github.com/luismartingarcia/protocol.git
            artwork(align='center' type='ascii-art' name='' alt='').
               0                   1                   2                   3
               0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
              +---------------------------------------------------------------+
              |  X264 NAL encoded payload                                     |
              +---------------------------------------------------------------+

      section(anchor="RGBD_Framing_Protocol")
        name RGBD Framing Protocol

        t
          | RGBD frames have variable size depending on its content, the first byte of the frame is a bit-mask
          | defining the frame content:
        table(align='center')
          thead
            tr
              th(align='center') Bit
              th(align='center') Frame Type
              th(align='center') Description
          tbody
            tr #[td 0] #[td RGB] #[td An RGB sub-frame is appended to the payload]
            tr #[td 1] #[td Depth] #[td A Depth sub-frame is appended to the payload]
            tr #[td 2] #[td Point-Cloud] #[td A Point-Cloud sub-frame is appended to the payload]

        t #[strong Note:] sub-frames are appended to the parent frame in the order described above (least significant bit first).

        section
          name RGBD Sub-Frames

          t(keepWithNext="true")
            | Each embedded sub-frame is encoded as follow:
          figure
            name RGBD sub-frame
            //- generated using https://github.com/luismartingarcia/protocol.git
            artwork(align='center' type='ascii-art' name='' alt='').
               0                   1                   2                   3
               0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
              +---------------------------------------------------------------+
              |   Frame Type  |            Frame Payload Size (u32)           |
              +---------------------------------------------------------------+
              |               | Frame Payload... (Frame Payload Size x bytes) |
              +---------------------------------------------------------------+

          table(align='center')
            thead
              tr
                th(align='center') Frame Type
                th(align='center') Value
                th(align='center') Frame Payload
            tbody
              tr #[td(rowspan="3") RGB] #[td 0x01] #[td The RGB Frame Payload contains a complete #[xref(target='JPEG_Frame') JPEG Frame]]
              tr                        #[td 0x02] #[td The RGB Frame Payload contains a complete #[xref(target='CV_MAT_Frame') CV_MAT Frame]]
              tr                        #[td 0x03] #[td The RGB Frame Payload contains a complete #[xref(target='X264_Frame') X264 Frame]]

              tr #[td(rowspan="2") Depth] #[td 0x01] #[td The Depth Frame Payload contains a #[xref(target='CV_MAT_Frame') CV_MAT Frame]]
              tr                          #[td 0x02] #[td The Depth Frame Payload contains an #[xref(target='LZ4_Frame') LZ4 compressed] CV_MAT Frame]

              tr #[td(rowspan="2") Point-Cloud] #[td 0x01] #[td The Point-Cloud Frame Payload contains a #[xref(target='PLY_Frame') PLY Frame]]
              tr                                #[td 0x02] #[td The Point-Cloud Frame Payload contains an #[xref(target='LZ4_Frame') LZ4 compressed] PLY Frame]

          section(anchor='PLY_Frame')
            name Point Cloud Frame

            t The content of this frame is a #[xref(target="PLY_FILE_FORMAT") PLY v.7 (binary format)] as implemented in the #[xref(target="POINT_CLOUD_LIBRARY") Point Cloud Library].

          section(anchor="LZ4_Frame")
            name LZ4 Compressed Frame

            t
              | The content of this frame is a #[xref(target='LZ4_Compression') LZ4] compressed payload of either a #[xref(target='CV_MAT_Frame') CV_MAT Frame] when used in a #[tt Depth Frame] or
              | a #[tt PLY Frame] when used in a #[tt Point-Cloud Frame].

    section
      name Contributors
      ul
        li= 'Alejandro Diaz Rosales <alejandro.diaz.rosales@cern.ch>'
        li= 'Mathieu Donze <Mathieu.Donze@cern.ch>'

  back

    references
      name References
      references
        name Normative References
        | &rfc793;
        | &rfc2119;
        | &rfc8259;

      references
        name URL References

        reference(anchor='CPP_CRF' target='https://gitlab.cern.ch/mro/robotics/cernroboticframework/cpproboticframework')
          front
            title CERN Robotic Framework - C++
            author
              organization CERN
            date(year='2019')

        reference(anchor='PY_CRF' target='https://gitlab.cern.ch/mro/robotics/cernroboticframework/pyroboticframework')
          front
            title CERN Robotic Framework - Python
            author
              organization CERN
            date(year='2020')

        reference(anchor='POSIX_base' target='https://ieeexplore.ieee.org/document/8277153')
          front
            title 1003.1-2017 - IEEE Standard for Information Technology--Portable Operating System Interface (POSIX(TM)) Base Specifications, Issue 7
            author
              organization IEEE Computed Society &amp; The Open Group
            date(year='2017')

        reference(anchor='OSI_MODEL' target='https://www.iso.org/standard/20269.html')
          front
            title ISO/IEC 7498-1:1994 - Information technology - Open Systems Interconnection - Basic Reference Model: The Basic Model
            author
              organization ISO/IEC JTC 1
            date(year='1994')

        reference(anchor="JSON_SCHEMA" target='https://json-schema.org/')
          front
            title JSON Schema is a vocabulary that allows you to annotate and validate JSON documents.
            author
              organization json-schema-org
            date(year='2009')

        reference(anchor="OPENRPC" target="https://open-rpc.org")
          front
            title The OpenRPC Specification defines a standard, programming language-agnostic interface description for JSON-RPC 2.0 APIs.
            author
              organization OpenRPC's opencollective
            date(year='2018')
          annotation #[tt License: Apache-2.0]

        reference(anchor="JSON_RPC" target="https://www.jsonrpc.org")
          front
            title JSON-RPC is a stateless, light-weight remote procedure call (RPC) protocol
            author
              organization JSON-RPC Working Group
            date(year='2005')
          annotation #[tt License: as-is]

        reference(anchor="POINT_CLOUD_LIBRARY" target="https://pointclouds.org/")
          front
            title The Point Cloud Library (PCL) is a standalone, large scale, open project for 2D/3D image and point cloud processing.
            author
              organization Point Cloud Library community
            date(year='2011')
          annotation #[tt License: BSD]

        reference(anchor="PLY_FILE_FORMAT" target="http://gamma.cs.unc.edu/POWERPLANT/papers/ply.pdf")
          front
            title The PLY Polygon File Format
            author(fullname='Greg Turk')
            date(year='1994')
          annotation #[tt License: BSD with non-commercial clause]

        reference(anchor="LZ4_Compression" target="http://www.lz4.org/")
          front
            title LZ4 lossless compression algorithm
            author(fullname='Yann Collet')
            date(year='2011')
          annotation #[tt License: BSD]

    section
      name Annex

      section(anchor='Introspect_Schema')
        name Introspect reply schema

        t
          | Here's a schema for the #[xref(target="Introspection") introspect] method:
        figure
          name Service introspection reply schema
          sourcecode(type="JSon" src="cern-robotic-framework-protocol/introspect-reply.schema.json")

      section(anchor='Schema_Validation')
        name Schema Validation

        t
          | To validate an introspection message, one can use #[tt jsonschema] Python library (amongst other choices):
        figure
          name Message validation
          sourcecode(type='bash').
            pip install 'jsonschema[format]'

            # validate a message using the schema file
            jsonschema -i ./service-introspection-reply.json ./introspect-reply.schema.json -o pretty

      section(anchor="Streaming_Introspection_Reply")
        name Streaming Introspection Reply

        t
          | Here's an example introspect reply for a streaming method:
        figure
          name Streaming method example
          sourcecode(type="JSon" src="cern-robotic-framework-protocol/service-streaming.json")


    //- section
    //-   name Change Log
    //-   t A list of changes

    //- section
    //-   name Open Issues
    //-   t A list of open issues regarding this document
